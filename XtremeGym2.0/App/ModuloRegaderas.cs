﻿using zkemkeeper;
using System.Collections.Generic;

namespace XtremeGym2._0.App
{
    class ModuloRegaderas
    {

        public CZKEMClass RegaderasHombres = new CZKEMClass();
        public CZKEMClass RegaderasMujeres = new CZKEMClass();

        private string IP_H = "192.168.1.203";
        private string IP_M = "192.168.1.202";
        private int DEFAULT_PORT = 4370;

        private string DEFAULT_USER = "999999";
        private string DEFAULT_ROLE = "Admin";
        private string DEFAULT_KEY_READER = "123";
        private int DEFAULT_COMMON_USER = 0;
        private int DEFAULT_SUPER_ADMIN = 3;

        internal bool connectTerminalMaleBath()
        {
            return RegaderasHombres.Connect_Net(IP_H, DEFAULT_PORT);
        }

        internal bool connectTerminalForFemaleBath()
        {
            return RegaderasMujeres.Connect_Net(IP_M, DEFAULT_PORT);
        }

        internal void configureReaderMale()
        {
            RegaderasHombres.BASE64 = 1;
        }

        internal void configureReaderFemale()
        {
            RegaderasMujeres.BASE64 = 1;
        }


        internal bool updateTerminalForMaleGender()
        {
            bool connect = connectTerminalMaleBath();
            configureReaderMale();
            bool clearReader = RegaderasHombres.ClearKeeperData(1);
            bool adminSet = RegaderasHombres.SSR_SetUserInfo(1, DEFAULT_USER, DEFAULT_ROLE, DEFAULT_KEY_READER, DEFAULT_SUPER_ADMIN, true);

            if (!connect) return false;
            if (!clearReader) return false;
            if (!adminSet) return false;

            Dictionary<string, List<string>> dataUserInformation = new Modelo().getGenderUserAndTemplatesForAccessInBathrooms(true);

            foreach (KeyValuePair<string, List<string>> itemInfo in dataUserInformation)
            {

                string userId = itemInfo.Key;
                List<string> fingerprings = itemInfo.Value;
                bool userInfo = RegaderasHombres.SSR_SetUserInfo(1, userId, "Usuario" + userId, DEFAULT_KEY_READER, DEFAULT_COMMON_USER, true);

                if (userInfo)
                {
                    for (int i = 0; i < fingerprings.Count; i++)
                    {
                        RegaderasHombres.SetUserTmpExStr(1, userId, i, 1, fingerprings[i]);
                    }
                }

            }

            return true;
        }

        internal bool updateTerminalForFemaleGender()
        {

            bool connect = connectTerminalForFemaleBath();
            configureReaderFemale();
            bool clearReader = RegaderasMujeres.ClearKeeperData(1);
            bool adminSet = RegaderasMujeres.SSR_SetUserInfo(1, DEFAULT_USER, DEFAULT_ROLE, DEFAULT_KEY_READER, DEFAULT_SUPER_ADMIN, true);

            if (!connect) return false;
            if (!clearReader) return false;
            if (!adminSet) return false;

            Dictionary<string, List<string>> dataUserInformation = new Modelo().getGenderUserAndTemplatesForAccessInBathrooms(false);

            foreach (KeyValuePair<string, List<string>> itemInfo in dataUserInformation)
            {

                string userId = itemInfo.Key;
                List<string> fingerprings = itemInfo.Value;
                bool userInfo = RegaderasMujeres.SSR_SetUserInfo(1, userId, "Usuario" + userId, DEFAULT_KEY_READER, DEFAULT_COMMON_USER, true);

                if (userInfo)
                {
                    for (int i = 0; i < fingerprings.Count; i++)
                    {
                        RegaderasMujeres.SetUserTmpExStr(1, userId, i, 1, fingerprings[i]);
                    }
                }

            }

            return true;
        }

    }

}
