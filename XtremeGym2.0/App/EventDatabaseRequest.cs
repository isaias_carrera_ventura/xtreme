﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using zkemkeeper;
using XtremeGym2._0.FStack;

namespace XtremeGym2._0.App
{
    class EventDatabaseRequest
    {
        MySqlConnection conexion = new MySqlConnection("Server=www.xtremegym.com.mx; Database=el99y94q_panel; Uid=el99y94q_panel;Pwd=kztM%.xE9sBl;");
        MySqlCommand sql;
        public static LogDevice log = new LogDevice();

        public void sendTorniqueteEvent(int reason, int doorOpen, string description)
        {
            string sql = "INSERT INTO `Event` (`idEvent`, `type`, `reason`, `date`, `hour`, `description`) VALUES (NULL, '"+ reason + "', '"+ doorOpen + "', CURDATE(), CURRENT_TIME, '"+ description+"');";
            //Si falla guardamos el log.
            if (!execQuery(sql)){

                log.appendToLog(LogDevice.LOG_EVENT,
                            reason,
                            doorOpen,
                            DateTime.Now.ToString("yyyy-MM-dd").ToString(),
                            DateTime.Now.ToString("HH:mm:ss").ToString(),
                            description);
            }

        }

        public void sendAccessEvent(int idUser, int door, bool access)
        {
            string sql = "INSERT INTO `Access` (`idAccess`, `idCliente`, `date`, `hour`, `type`, `door`, `access`) VALUES (NULL, '"+ idUser + "', CURDATE(), CURRENT_TIME, '0', '"+ door + "', '"+(access ? 1 : 0) +"');";
            
            if (!execQuery(sql)){
                
                log.appendToLog(LogDevice.LOG_ACCESS,
                            idUser,
                            door,
                            access,
                            DateTime.Now.ToString("yyyy-MM-dd").ToString(),
                            DateTime.Now.ToString("HH:mm:ss").ToString());

            }

            if (door == 2 && access)
            {
                registerAssistantForUser(idUser);
            }

        }

        internal void registerAssistantForUser(int id)
        {
            try
            {
                int assistants = getAssistantsForUser(id);

                if (assistants > 0)
                {
                    return;
                }

                string sql = "INSERT INTO CarroCompras (IdCliente,Fecha,Usuario,Estado,Status,IdUsuarioModifico,FechaModifico) values(" + id + ", CURDATE(), 0, 1, 1, 0, CURDATE());";
                execQuery(sql);
            }
            catch (Exception)
            {
            }

        }
        internal void registerAssistantForUser(int id, string date)
        {
            try
            {
                int assistants = getAssistantsForUser(id, date);

                if (assistants > 0)
                {
                    return;
                }

                string sql = "INSERT INTO CarroCompras (IdCliente,Fecha,Usuario,Estado,Status,IdUsuarioModifico,FechaModifico) values(" + id + ", '"+ date + "', 0, 1, 1, 0, '" + date + "');";
                execQuery(sql);
            }
            catch (Exception)
            {
            }

        }

        internal int getAssistantsForUser(int idUser, string date)
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("SELECT count(*) as total FROM CarroCompras WHERE Status=1 AND Fecha='"+ date + "' AND IdCliente='" + idUser + "'", conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);

            conexion.Close();

            int assistants = int.Parse(dt.Rows[0][0].ToString());

            return assistants;
        }

        internal int getAssistantsForUser(int idUser)
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("SELECT count(*) as total FROM CarroCompras WHERE Status=1 AND Fecha=CURDATE() AND IdCliente='" + idUser + "'", conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);

            conexion.Close();

            int assistants = int.Parse(dt.Rows[0][0].ToString());

            return assistants;

        }

        public bool execQuery(string query)
        {
            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }

            conexion.Open();
            sql = new MySqlCommand(query, conexion);
            try
            {
                sql.ExecuteNonQuery();

                conexion.Close();
                return true;
            }
            catch (Exception ex)
            {
                conexion.Close();
                return false;
            }
        }

        public async void sendLogToServer()
        {
            List<string> lines = log.getLogEvents();
            List<string> linesToOverride = new List<string>();
            if (lines != null)
            {
                if (lines.Count > 0)
                {
                    for (int i = 0; i < lines.Count; i++)
                    {

                        string line = lines[i];
                        string[] logRaw = line.Split(';');
                        int logInt = int.Parse(logRaw[0]);

                        if (logInt == LogDevice.LOG_EVENT)
                        {

                            int reason = int.Parse(logRaw[1]);
                            int type = int.Parse(logRaw[2]);
                            string date = logRaw[3];
                            string time = logRaw[4];
                            string description = logRaw[5];

                            string sql = "INSERT INTO `Event` (`idEvent`, `type`, `reason`, `date`, `hour`, `description`) VALUES (NULL, '" + reason + "', '" + type + "', '"+ date + "', '"+ time + "', '" + description + "');";
                            
                            if (!execQuery(sql))
                            {
                                linesToOverride.Add(lines[i]);
                            }

                        }
                        else if (logInt == LogDevice.LOG_ACCESS)
                        {

                            int idUser = int.Parse(logRaw[1]);
                            int door = int.Parse(logRaw[2]);
                            bool flagStatus = bool.Parse(logRaw[3]);
                            string date = logRaw[4];
                            string time = logRaw[5];

                            string sql = "INSERT INTO `Access` (`idAccess`, `idCliente`, `date`, `hour`, `type`, `door`, `access`) VALUES (NULL, '" + idUser + "', '" + date + "', '" + time + "', '0', '" + door + "', '" + (flagStatus ? 1 : 0) + "');";
                            
                            if (!execQuery(sql))
                            {
                                linesToOverride.Add(lines[i]);
                            }

                            if (door == 2)
                            {
                                registerAssistantForUser(idUser,date);
                            }
                        }

                    }

                    log.rewriteArrayOfLogs(linesToOverride);

                }
            }
        }

    }
}
