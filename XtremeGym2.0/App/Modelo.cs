﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using zkemkeeper;
using XtremeGym2._0.FStack;

namespace XtremeGym2._0
{
    public class Modelo
    {
        MySqlConnection conexion = new MySqlConnection("Server=www.xtremegym.com.mx; Database=el99y94q_panel; Uid=el99y94q_panel;Pwd=kztM%.xE9sBl;");
        MySqlCommand sql;
        private const int DAYS_ALLOWED = 3;

        public Modelo()
        {

        }


        internal object getUsuariosByName(string text)
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("select * from (Select IdCliente,Concat(Nombre,' ',ApellidoPaterno,' ',ApellidoMaterno)as NOMBRE,Edad,Sexo,Correo,Telefono from Clientes) T" +
                " where NOMBRE like '%" + text + "%' ", conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);


            conexion.Close();

            return dt;
        }

        internal object getIdUsuarioByTemplate(int id)
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("Select tem.IdCliente, cli.Nombre from TemplatesClientes tem inner join Clientes cli on tem.IdCliente = cli.IdCliente where IdTemplateCliente =" + id + ";", conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);


            conexion.Close();

            return dt;
        }

        internal object getIdCarro(int id)
        {
            string fecha = DateTime.Now.ToString("yyyy/MM/dd");
            string hora = DateTime.Now.ToShortTimeString();

            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("select count(Status) as total from CarroCompras where IdCliente=" + id + " and Fecha='" + fecha + "' and Status=1;", conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);


            conexion.Close();

            return dt;

        }


        internal object getIdEmpleado(int id)
        {
            string fecha = DateTime.Now.ToString("yyyy/MM/dd");
            string hora = DateTime.Now.ToShortTimeString();

            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("select count(Status) as total, Salida from Horarios where IdCliente=" + id + " and Fecha='" + fecha + "' and Status=1;", conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);


            conexion.Close();

            return dt;

        }


        internal object getIdEmpleadoHuella(int id)
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("select IdCliente from TemplatesClientes where IdTemplateCliente=" + id + " and Status=1;", conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);


            conexion.Close();

            return dt;

        }

        internal DataTable getHuellasById(int iD)
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("Select * from TemplatesClientes where IdCliente=" + iD, conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);


            conexion.Close();

            return dt;
        }

        internal DataTable getHuellasByIdAndFinger(int iD, int idFinger)
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("Select * from TemplatesClientes where IdCliente=" + iD + " and Finger=" + idFinger, conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);


            conexion.Close();

            return dt;
        }

        internal DataTable getUsuarioById(int iD)
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("Select * from Clientes where IdCliente=" + iD, conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);


            conexion.Close();

            return dt;
        }

        internal DataTable getUsuarios()
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("Select IdCliente,Concat(Nombre,' ',ApellidoPaterno,' ',ApellidoMaterno)as Nombre,Edad,Sexo,Correo,Telefono from Clientes", conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);


            conexion.Close();

            return dt;
        }

        internal DataTable ObtenerPaqueteById(int iD)
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("Select p.Paquete,p.Visitas,pc.FechaInicio,pc.FechaFinal from PaquetesClientes pc JOIN Paquetes p ON p.IdPaquete = pc.IdPaquete where pc.IdCliente =" + iD + ";", conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);
            conexion.Close();

            return dt;
        }

        internal bool isUserEmployee(int idUser)
        {

            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("select * from Usuarios where IdCliente="+ idUser + ";", conexion);
            DataTable dt = new DataTable();

            adapter.Fill(dt);
            conexion.Close();

            return dt.Rows.Count > 0;

        }

        //[DllImport("C:\\WINDOWS\\system32\\plcommpro.dll", EntryPoint = "Connect")]
        //public static extern IntPtr Connect(string Parameters);
        //[DllImport("plcommpro.dll", EntryPoint = "PullLastError")]
        //public static extern int PullLastError();
        //IntPtr h = IntPtr.Zero;

        public CZKEMClass Terminal1;
        public CZKEMClass Terminal2;

        public int IdCliente = 0;
        bool conectado1 = false;
        bool conectado2 = false;

        internal void subirTemplate(string v, int iD, int finger)
        {
            string sql = "INSERt INTO TemplatesClientes (IdCliente,Template,Status,finger) values('" + iD + "','" + v + "',1,'" + finger + "');";
            Exec_query(sql);
        }

        public bool Exec_query(string consulta)
        {
            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }

            conexion.Open();
            sql = new MySqlCommand(consulta, conexion);
            try
            {
                sql.ExecuteNonQuery();

                conexion.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message.ToString());

                conexion.Close();
                return false;
            }

        }

        public int Select_id_registrado(string consulta)
        {
            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }

            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter(consulta, conexion);

            DataTable dt = new DataTable();

            adapter.Fill(dt);
            DataRow categoryRow = dt.Rows[0];

            int id;
            if (dt.Rows.Count > 0)
            {
                id = Convert.ToInt32(categoryRow["IdCliente"]);
            }
            else
            {
                id = 0;
            }


            conexion.Close();

            return id;

        }



        public bool Correo_query(string correo, string accion)
        {
            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }

            bool flag = true;
            string consulta = "";
            if (accion == "agregar")
            {
                consulta = "Select * from Clientes WHERE Clientes.Correo='" + correo + "';";
            }
            else if (accion == "editar")
            {
                consulta = "Select * from Clientes WHERE Clientes.Correo='" + correo + "' AND Clientes.Correo <> '" + correo + "';";
            }
            conexion.Open();
            sql = new MySqlCommand();

            sql.CommandText = consulta;
            sql.Connection = conexion;

            MySqlDataReader mr = sql.ExecuteReader();



            if (mr.HasRows == true)
            {
                flag = false;
            }
            else
            {
                flag = true;
            }

            /*Si es true, no existe el correo, si es false, existe*/

            conexion.Close();

            return flag;
        }

        public string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        //[DllImport("plcommpro.dll", EntryPoint = "SetDeviceData")]
        //public static extern int SetDeviceData(IntPtr h, string tablename, string data, string options);

        public bool Actualizar_Usuario_Cliente(int idcliente, int idtemplate1, int idtemplate2, string img, string nombre, string AP, string AM, int edad, string sexo, string correo, string telefono, string calle, string numero, string colonia, string cp, string ciudad, string clave, int orden, int status, string template, string template2)
        {
            bool exito = false;
            bool update1 = false;
            bool update2 = false;
            string sql = "";

            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }

            if (img != null)
            {
                sql = "UPDATE Clientes SET Imagen='" + img + "', Nombre='" + nombre + "', ApellidoPaterno='" + AP + "', ApellidoMaterno='" + AM + "', Edad=" + edad + ", Sexo='" + sexo + "', Correo='" + correo + "', Telefono='" + telefono + "', Calle='" + calle + "', Numero='" + numero + "', Colonia='" + colonia + "', CP='" + cp + "', Ciudad='" + ciudad + "' WHERE IdCliente=" + idcliente + ";";

            }
            else if (img == null)
            {
                sql = "UPDATE Clientes SET Nombre='" + nombre + "', ApellidoPaterno='" + AP + "', ApellidoMaterno='" + AM + "', Edad=" + edad + ", Sexo='" + sexo + "', Correo='" + correo + "', Telefono='" + telefono + "', Calle='" + calle + "', Numero='" + numero + "', Colonia='" + colonia + "', CP='" + cp + "', Ciudad='" + ciudad + "' WHERE IdCliente=" + idcliente + ";";

            }

            if (Exec_query(sql) == true)
            {

                if (idtemplate1 != 0)
                {
                    sql = "UPDATE TemplatesClientes SET Template='" + template + "' WHERE IdCliente=" + idcliente + " AND IdTemplateCliente=" + idtemplate1 + ";";
                    update1 = Exec_query(sql);
                }
                else
                {
                    sql = "INSERt INTO TemplatesClientes (IdCliente,Template,Status) values('" + idcliente + "','" + template + "',1);";
                    update1 = Exec_query(sql);
                }

                if (idtemplate2 != 0)
                {
                    sql = "UPDATE TemplatesClientes SET Template='" + template2 + "' WHERE IdCliente=" + idcliente + " AND IdTemplateCliente=" + idtemplate2 + ";";
                    update2 = Exec_query(sql);
                }
                else
                {
                    sql = "INSERt INTO TemplatesClientes (IdCliente,Template,Status) values('" + idcliente + "','" + template2 + "',1);";
                    update2 = Exec_query(sql);
                }

            }


            exito = true;
            MessageBox.Show("Exito al Actualizar Todos los Datos.");


            return exito;
        }

        internal void Registro_Horario(int idusuario, int tipo)
        {

            string fecha = DateTime.Now.ToString("yyyy/MM/dd");
            string fechahora = DateTime.Now.ToString("yyyy/MM/dd");
            string hora = DateTime.Now.ToShortTimeString();
            string sql = "";

            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }

            if (tipo < 1)
            {

                sql = "INSERT INTO Horarios (IdCliente,Fecha,Entrada,Status,IdUsuarioModifico,FechaModifico) values(" + idusuario + ",'" + fecha + "','" + hora + "',1 ," + idusuario + ", '" + fechahora + "');";
                Exec_query(sql);

            }
            else
            {

                sql = "UPDATE Horarios SET Salida='" + hora + "', IdUsuarioModifico=" + idusuario + ", FechaModifico='" + fecha + "'  WHERE IdCliente=" + idusuario + " and Fecha = '" + fechahora + "';";
                Exec_query(sql);

            }

        }

        internal void setScheduleForEmployee(int idUsuario)
        {

            string fecha = DateTime.Now.ToString("yyyy/MM/dd");
            string fechahora = DateTime.Now.ToString("yyyy/MM/dd");
            string hora = DateTime.Now.ToShortTimeString();

            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("select * from Horarios where IdCliente=" + idUsuario+" and Fecha='"+ fecha + "' order by IdHorario desc limit 1;", conexion);
            DataTable dt = new DataTable();
            adapter.Fill(dt);

            //Hay ya un horario registrado el día de hoy.
            if (dt.Rows.Count > 0)
            {

                string salida = dt.Rows[0][4].ToString();
                int idHorario = int.Parse(dt.Rows[0][0].ToString());

                //No hay salida.
                if (salida == string.Empty)
                {

                    string sql = "UPDATE Horarios SET Salida='" + hora + "', IdUsuarioModifico=" + idUsuario + ", FechaModifico='" + fecha + "'  WHERE IdCliente=" + idUsuario + " and IdHorario = " + idHorario + ";";
                    Exec_query(sql);

                }
                //Crear un nuevo horario.
                else
                {

                    string sql = "INSERT INTO Horarios (IdCliente,Fecha,Entrada,Status,IdUsuarioModifico,FechaModifico) values(" + idUsuario + ",'" + fecha + "','" + hora + "',1 ," + idUsuario + ", '" + fechahora + "');";
                    Exec_query(sql);

                }

            }
            else
            {

                //No hay horario registrado. (Registrar uno nuevo y actualizar entrada.)
                string sql = "INSERT INTO Horarios (IdCliente,Fecha,Entrada,Status,IdUsuarioModifico,FechaModifico) values(" + idUsuario + ",'" + fecha + "','" + hora + "',1 ," + idUsuario + ", '" + fechahora + "');";
                Exec_query(sql);

            }

            conexion.Close();

        }

        internal void Registro_Carro(int id)
        {
            string fecha = DateTime.Now.ToString("yyyy/MM/dd");
            string fechahora = DateTime.Now.ToString("yyyy/MM/dd");
            string hora = DateTime.Now.ToShortTimeString();
            string sql = "";

            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }

            sql = "INSERT INTO CarroCompras (IdCliente,Fecha,Usuario,Estado,Status,IdUsuarioModifico,FechaModifico) values(" + id + ",'" + fecha + "', 0, 1, 1, 0, '" + fechahora + "');";
            Exec_query(sql);
        }

        public bool Registro_Usuario_Cliente(string img, string nombre, string AP, string AM, int edad, string sexo, string correo, string telefono, string calle, string numero, string colonia, string cp, string ciudad, string clave, int orden, int status, string template, string template2)
        {
            if (conexion.State == ConnectionState.Open)
            {
                conexion.Close();
            }

            bool exito = false;
            DateTime date = new DateTime();
            string fecha = date.Date.ToString();
            string sql = "INSERT INTO Clientes (Imagen,Nombre,ApellidoPaterno,ApellidoMaterno,Edad,Sexo,Correo,Telefono,Calle,Numero,Colonia,Ciudad,CP,Clave,Orden,Status) VALUES('" + img + "','" + nombre + "','" + AP + "','" + AM + "'," + edad + ",'" + sexo + "','" + correo + "'," + telefono + ",'" + calle + "','" + numero + "','" + colonia + "','" + cp + "','" + ciudad + "','" + clave + "'," + orden + "," + status + ");";

            int ret = 0;
            string data = "";
            string options = "";

            if (Exec_query(sql) == true)
            {
                int id = Select_id_registrado("SELECT IdCliente FROM Clientes where Correo='" + correo + "';");
                sql = "INSERt INTO TemplatesClientes (IdCliente,Template,Status) values('" + id + "','" + template + "',1);";
                string sql2 = "INSERt INTO TemplatesClientes (IdCliente,Template,Status) values('" + id + "','" + template2 + "',1);";

                if (Exec_query(sql) == true && Exec_query(sql2) == true)
                {
                    exito = true;
                }
                else
                {
                    exito = false;
                }
            }
            else
            {
                exito = false;
            }
            return exito;
        }

        public void Cancelar(UserControl us)
        {

            Control groupbox;
            for (int i = us.Controls.Count - 1; i >= 0; i--)
            {
                groupbox = us.Controls[i];
                if (groupbox is GroupBox)
                {
                    for (int index = groupbox.Controls.Count - 1; index >= 0; index--)
                    {
                        if (groupbox.Controls[index] is TextBox)
                        {
                            if (groupbox.Controls[index].Text.Length >= 0)
                            {
                                groupbox.Controls[index].ResetText();
                            }
                        }
                    }
                }
            }
        }


        //true Male
        //false Female
        public Dictionary<string, List<string>> getGenderUserAndTemplatesForAccessInBathrooms(bool genderToConsult)
        {

            string consultaExec;

            if (genderToConsult)
            {
                consultaExec = "Select distinct cli.IdCliente from PaquetesClientes paq inner join Clientes cli on paq.IdCliente = cli.IdCliente INNER JOIN Paquetes as P ON P.IdPaquete = paq.IdPaquete INNER JOIN TemplatesClientes as TC on TC.IdCliente = cli.IdCliente where cli.Sexo = 'M' and cli.Status = 1 AND NOW() between paq.FechaInicio and paq.FechaFinal AND P.Regadera=1;";
            }
            else{
                consultaExec = "Select distinct cli.IdCliente from PaquetesClientes paq inner join Clientes cli on paq.IdCliente = cli.IdCliente INNER JOIN Paquetes as P ON P.IdPaquete = paq.IdPaquete INNER JOIN TemplatesClientes as TC on TC.IdCliente = cli.IdCliente where cli.Sexo = 'F' and cli.Status = 1 AND NOW() between paq.FechaInicio and paq.FechaFinal AND P.Regadera=1;";
            }

            MySqlDataAdapter adapter1 = new MySqlDataAdapter(consultaExec, conexion);
            DataTable dt_hombre = new DataTable();
            adapter1.Fill(dt_hombre);

            Dictionary<string, List<string>> data = new Dictionary<string, List<string>>();

            for (int i = 0; i < dt_hombre.Rows.Count; i = i + 1)
            {

                string idUser = dt_hombre.Rows[i][0].ToString();

                List<string> fingerprints = new List<string>();
                DataTable huellas = this.getHuellasById(int.Parse(idUser));

                for(int j = 0; j < huellas.Rows.Count; j = j + 1)
                {
                    fingerprints.Add(huellas.Rows[j][2].ToString());
                }

                data.Add(idUser, fingerprints);

            }

            return data;

        }

        public bool Validacion2(UserControl us)
        {
            bool flag = false;

            Control groupbox;
            for (int i = us.Controls.Count - 1; i >= 0; i--)
            {
                groupbox = us.Controls[i];
                if (groupbox is GroupBox)
                {
                    for (int index = groupbox.Controls.Count - 1; index >= 0; index--)
                    {
                        if (groupbox.Controls[index] is TextBox && groupbox.Controls[index].Name != "txt_NoCliente")
                        {
                            if (String.IsNullOrEmpty(groupbox.Controls[index].Text.ToString()) || String.IsNullOrWhiteSpace(groupbox.Controls[index].Text.ToString()))
                            {
                                string result = groupbox.Controls[index].Name.Substring(groupbox.Controls[index].Name.LastIndexOf('_') + 1);
                                flag = true;
                                MessageBox.Show("El campo: " + result + " no puede estar vacio.");
                                return true;

                            }
                            else
                            {
                                flag = false;
                            }
                        }
                    }
                }
            }

            return flag;
        }

        public int getUserValidityAsStatus(int id)
        {
            DataTable t = ObtenerPaqueteById(id);

            int final = t.Rows.Count - 1;

            if (final >= 0)
            {

                int añoFinal = int.Parse(t.Rows[final][3].ToString().Substring(6, 4));
                int mesFinal = int.Parse(t.Rows[final][3].ToString().Substring(3, 2));
                int diaFinal = int.Parse(t.Rows[final][3].ToString().Substring(0, 2));

                DateTime finalDate = new DateTime(añoFinal, mesFinal, diaFinal);

                DateTime dateAllowedInitial = new DateTime(añoFinal, mesFinal, diaFinal).AddDays(-DAYS_ALLOWED);
                DateTime dateAllowedFinal = new DateTime(añoFinal, mesFinal, diaFinal).AddDays(DAYS_ALLOWED);

                if (DateTime.Now >= dateAllowedInitial && DateTime.Now <= dateAllowedFinal)
                {
                    return Constants.ACCESS_ABOUT_TO_EXPIRE;
                }
                else if (finalDate >= DateTime.Now)
                {
                    return Constants.ACCESS_SUCCESS;
                }
                else
                {
                    return Constants.ACCESS_EXPIRED;
                }

            }
            else
            {
                return Constants.ACCESS_NO_PACKAGE_REGISTERED;
            }
        }


        /**
         * Obtiene las huellas de los usuarios
         */
        public static Templete[] Templetes()
        {

            MySqlConnection connection = new MySqlConnection("Server=www.xtremegym.com.mx; Database=el99y94q_panel; Uid=el99y94q_panel; Pwd=kztM%.xE9sBl;");
            connection.Open();
            String cmd = "select IdTemplateCliente,Template from TemplatesClientes";
            MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(cmd, connection);
            mySqlDataAdapter.SelectCommand.CommandType = System.Data.CommandType.Text;
            DataTable dt = new DataTable();
            mySqlDataAdapter.Fill(dt);

            Templete[] templetes = new Templete[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                templetes[i] = new Templete(dt.Rows[i][1].ToString(), dt.Rows[i][0].ToString());
            }

            connection.Close();
            return templetes;
        }

        /**
         * Obtiene las huellas de los empleados.
         */
        public static Templete[] TempletesEmpleado()
        {

            MySqlConnection connection = new MySqlConnection("Server=www.xtremegym.com.mx; Database=el99y94q_panel; Uid=el99y94q_panel; Pwd=kztM%.xE9sBl;");
            connection.Open();
            String cmd = "select tem.IdTemplateCliente, tem.Template from TemplatesClientes as tem inner join Usuarios as usu on tem.IdCliente = usu.IdCliente";
            MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(cmd, connection);
            mySqlDataAdapter.SelectCommand.CommandType = System.Data.CommandType.Text;
            DataTable dt = new DataTable();
            mySqlDataAdapter.Fill(dt);

            Templete[] TempletesEmpleado = new Templete[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                TempletesEmpleado[i] = new Templete(dt.Rows[i][1].ToString(), dt.Rows[i][0].ToString());
            }

            connection.Close();
            return TempletesEmpleado;
        }


        public bool discardFingerprintsForUser(int id)
        {

            string sql = "DELETE FROM TemplatesClientes WHERE TemplatesClientes.IdCliente="+id+";";
            return Exec_query(sql);

        }

        /**
         * Obtiene las fechas de vencimiento de los usuarios
         */
        public static Dictionary<string, string> getInitialAndFinalDatesForUserId(int id)
        {

            Modelo modelo = new Modelo();
            DataTable t = modelo.ObtenerPaqueteById(id);
            int final = t.Rows.Count - 1;

            if (final >= 0)
            {

                int añoInicial = int.Parse(t.Rows[final][2].ToString().Substring(6, 4));
                int mesInicial = int.Parse(t.Rows[final][2].ToString().Substring(3, 2));
                int diaInicial = int.Parse(t.Rows[final][2].ToString().Substring(0, 2));

                int añoFinal = int.Parse(t.Rows[final][3].ToString().Substring(6, 4));
                int mesFinal = int.Parse(t.Rows[final][3].ToString().Substring(3, 2));
                int diaFinal = int.Parse(t.Rows[final][3].ToString().Substring(0, 2));

                //Fecha final + DAYS_ALLOWED días de compensación.
                DateTime date = new DateTime(añoFinal, mesFinal, diaFinal);
                date = date.AddDays(DAYS_ALLOWED);

                int year = date.Year;
                int month = date.Month;
                int day = date.Day;

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                string mesInicialStr = (mesInicial < 10 ? ("0" + mesInicial) : ("" + mesInicial));
                string diaInicialStr = (diaInicial < 10 ? ("0" + diaInicial) : ("" + diaInicial));

                dictionary.Add("initialDate", añoInicial + "" + mesInicialStr + "" + diaInicialStr);

                string mesFinalStr = (month < 10 ? ("0" + month) : ("" + month));
                string diaFinalStr = (day < 10 ? ("0" + day) : ("" + day));

                dictionary.Add("finalDate", year + "" + mesFinalStr + "" + diaFinalStr);

                return dictionary;
            }
            else
            {
                return null;
            }

        }

        internal List<string> getIDListActiveUsers()
        {

            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("Select DISTINCT cli.IdCliente from PaquetesClientes paq inner join Clientes cli on paq.IdCliente = cli.IdCliente where cli.Status = 1 AND NOW() between paq.FechaInicio and paq.FechaFinal ORDER BY cli.IdCliente ASC ", conexion);
            DataTable dt = new DataTable();

            adapter.Fill(dt);

            List<string> idList = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                idList.Add(dt.Rows[i][0].ToString());
            }


            conexion.Close();

            return idList;

        }

        internal List<string> getIDListActiveEmployees()
        {
            conexion.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("Select distinct IdCliente from Usuarios where Status=1 and IdCliente is not null", conexion);
            DataTable dt = new DataTable();

            adapter.Fill(dt);

            List<string> idList = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                try
                {
                    idList.Add(dt.Rows[i][0].ToString());
                }
                catch (Exception)
                {

                }
            }


            conexion.Close();

            return idList;
        }


    }

    public class Templete
    {
        string templeteString;
        string id;

        public Templete(string templeteString, string id)
        {
            this.templeteString = templeteString;
            this.id = id;
        }

        public string TempleteString { get => templeteString; set => templeteString = value; }
        public string Id { get => id; set => id = value; }
    }



}
