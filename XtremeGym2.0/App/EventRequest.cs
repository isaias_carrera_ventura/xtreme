﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using XtremeGym2._0.App;

namespace XtremeGym2._0.FStack
{

    class EventRequest
    {

        #region EVENT MANUAL

        public static LogDevice log = new LogDevice();

        /**
         * Envia una peticion al servidor.
         */
        /*public void sendTourniquetEvent(int reason, int doorOpen, string description)
        {

            //Paramétros para el API
            IEnumerable<KeyValuePair<string, string>> queries = new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>("reason",reason.ToString()),
                new KeyValuePair<string,string> ("type",doorOpen.ToString()),
                new KeyValuePair<string,string> ("description",description)
            };

            postTourniquetEventRequest(queries);

        }*/

        /**
         * Crea una llamada para abrir y cerrar la puerta
         */
        /*async static void postTourniquetEventRequest(IEnumerable<KeyValuePair<string, string>> queries)
        {

            //hacemos la llamada
            HttpContent formValues = new FormUrlEncodedContent(queries);
            using (HttpClient client = new HttpClient())
            {

                using (HttpResponseMessage response = await client.PostAsync(Constants.URL_EVENT_TOURNIQUET_REQUEST, formValues))
                {
                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {

                        log.appendToLog(LogDevice.LOG_EVENT, 
                            int.Parse(queries.ElementAt(0).Value), 
                            int.Parse(queries.ElementAt(1).Value), 
                            DateTime.Now.ToString("yyyy-MM-dd").ToString(), 
                            DateTime.Now.ToString("HH:mm:ss").ToString(), 
                            queries.ElementAt(2).Value);

                    }

                }

            }
        }
         */

        /**
         * Crea una llamada para abrir y cerrar la puerta
         */
        /*async static Task<bool> PostTourniquetEventRequest(IEnumerable<KeyValuePair<string, string>> queries)
        {

            //hacemos la llamada
            HttpContent formValues = new FormUrlEncodedContent(queries);
            using (HttpClient client = new HttpClient())
            {

                using (HttpResponseMessage response = await client.PostAsync(Constants.URL_EVENT_TOURNIQUET_REQUEST, formValues))
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        return true;
                    }

                    return false;
                }

            }
        }
        */
        #endregion EVENT

        #region EVENT ACCESS

        /**
         * Envia una peticion de acceso a un usuario al servidor.
         */
        /*public void sendAccessEvent(int idUser, int door, bool access)
        {

            //Paramétros para el API
            IEnumerable<KeyValuePair<string, string>> queries = new List<KeyValuePair<string, string>>() {

                new KeyValuePair<string, string>("id_cliente",idUser.ToString()),
                new KeyValuePair<string,string> ("door",door.ToString()),
                new KeyValuePair<string,string> ("access",access.ToString())

            };

            postAccessEventRequest(queries);

        }*/

        /**
         * Crea una llamada para acces de  usuario
         */
        /*private async void postAccessEventRequest(IEnumerable<KeyValuePair<string, string>> queries)
        {
            //Hacemos la llamada
            HttpContent formValues = new FormUrlEncodedContent(queries);
            using (HttpClient client = new HttpClient())
            {

                using (HttpResponseMessage response = await client.PostAsync(Constants.URL_EVENT_ACCESS_REQUEST, formValues))
                {

                    if (response.StatusCode != System.Net.HttpStatusCode.OK)
                    {

                        log.appendToLog(LogDevice.LOG_ACCESS, int.Parse(queries.ElementAt(0).Value),
                            int.Parse(queries.ElementAt(1).Value),
                            bool.Parse(queries.ElementAt(2).Value),
                            DateTime.Now.ToString("yyyy-MM-dd").ToString(),
                            DateTime.Now.ToString("HH:mm:ss").ToString());

                    }

                }

            }

        }
        */

        /**
         * Crea una llamada para acces de  usuario
         */
        /*
        private async Task<bool> PostAccessEventRequest(IEnumerable<KeyValuePair<string, string>> queries)
        {
            //Hacemos la llamada
            HttpContent formValues = new FormUrlEncodedContent(queries);
            using (HttpClient client = new HttpClient())
            {

                using (HttpResponseMessage response = await client.PostAsync(Constants.URL_EVENT_ACCESS_REQUEST, formValues))
                {

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        return true;
                    }

                    return false;

                }

            }

        }*/


        #endregion EVENT ACCESS

        /*
    public async void sendLogToServer()
    {

        LogDevice log = new LogDevice();
        List<string> lines = log.getLogEvents();
        List<string> linesToOverride = new List<string>();

        if (lines != null)
        {
            if (lines.Count > 0)
            {
                for (int i = 0; i < lines.Count; i++)
                {

                    string line = lines[i];
                    string[] logRaw = line.Split(';');
                    int logInt = int.Parse(logRaw[0]);

                    if (logInt == LogDevice.LOG_EVENT)
                    {

                        int reason = int.Parse(logRaw[1]);
                        int type = int.Parse(logRaw[2]);
                        string date = logRaw[3];
                        string time = logRaw[4];
                        string description = logRaw[5];

                        //Paramétros para el API
                        IEnumerable<KeyValuePair<string, string>> queries = new List<KeyValuePair<string, string>>() {

                            new KeyValuePair<string, string>("reason",reason.ToString()),
                            new KeyValuePair<string,string> ("type",type.ToString()),
                            new KeyValuePair<string,string> ("date",date),
                            new KeyValuePair<string,string> ("hour",time),
                            new KeyValuePair<string,string> ("description",description)

                        };

                        bool result = await PostTourniquetEventRequest(queries);

                        if (!result)
                        {
                            linesToOverride.Add(lines[i]);
                        }

                    }
                    else if (logInt == LogDevice.LOG_ACCESS)
                    {

                        int idUser = int.Parse(logRaw[1]);
                        int door = int.Parse(logRaw[2]);
                        bool flagStatus = bool.Parse(logRaw[3]);
                        string date = logRaw[4];
                        string time = logRaw[5];

                        IEnumerable<KeyValuePair<string, string>> queries = new List<KeyValuePair<string, string>>() {

                            new KeyValuePair<string, string>("id_cliente",idUser.ToString()),
                            new KeyValuePair<string,string> ("door",door.ToString()),
                            new KeyValuePair<string,string> ("access",flagStatus.ToString()),
                            new KeyValuePair<string,string> ("date",date),
                            new KeyValuePair<string,string> ("hour",time)

                        };

                        bool result = await PostAccessEventRequest(queries);

                        if (!result)
                        {
                            linesToOverride.Add(lines[i]);
                        }

                    }

                }

                log.rewriteArrayOfLogs(linesToOverride);

            }
        }

    }*/


    }
}
