﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using libzkfpcsharp;
using Sample;

namespace XtremeGym2._0
{
    public class fingerprint
    {
        public static Thread captureThread;
        string accion = "identificar";
        string templete = "";
        private PictureBox pictureBox;
        public static bool isInitialized = false;
        bool bIsTimeToDie = false;
        public static IntPtr mDevHandle = IntPtr.Zero;
        public static IntPtr mDBHandle = IntPtr.Zero;
        bool IsRegister = false;
        bool bIdentify = true;
        byte[] FPBuffer;
        int RegisterCount = 0;
        const int REGISTER_FINGER_COUNT = 3;

        int IdHuellaCliente = 0;
        bool salir = false;
        int error = 0;

        byte[][] RegTmps = new byte[3][];
        byte[] RegTmp = new byte[2048];
        byte[] CapTmp = new byte[2048];
        int cbCapTmp = 2048;
        int cbRegTmp = 0;
        int iFid = 1;

        private int mfpWidth = 0;
        private int mfpHeight = 0;

        public int CbCapTmp { get => cbCapTmp; set => cbCapTmp = value; }
        public byte[] CapTmp1 { get => CapTmp; set => CapTmp = value; }
        public string Fp_reg { get => templete; set => templete = value; }
        public PictureBox PictureBox { get => pictureBox; set => pictureBox = value; }
        public int IdHuellaCliente1 { get => IdHuellaCliente; set => IdHuellaCliente = value; }
        public bool Salir { get => salir; set => salir = value; }
        public int Error { get => error; set => error = value; }

        public fingerprint()
        {
            
            if (!isInitialized)
            {/*
                if (inicarDispositivo())
                {
                    isInitialized = true;
                    cargarTemplates(Modelo.Templetes());

                }
                else
                {
                    MessageBox.Show("Error al inicializar el lector, desconectelo y reinicie el programa.");
                }
                */
            }
        }

        static void ShowMessage(string message,
        [CallerLineNumber] int lineNumber = 0,
        [CallerMemberName] string caller = null)
            {
                MessageBox.Show(message + " at line " + lineNumber + " (" + caller + ")");
            }

        public bool inicarDispositivo()
        {
            bool res = false ;
            if (zkfp2.Init() == zkfp.ZKFP_ERR_OK)
            {
                mDevHandle = zkfp2.OpenDevice(0);
                if (mDevHandle != IntPtr.Zero)
                {
                    mDBHandle = zkfp2.DBInit();
                    if (mDBHandle != IntPtr.Zero)
                    {
                        RegisterCount = 0;
                        cbRegTmp = 0;
                        iFid = 1;
                        for (int i = 0; i < 3; i++)
                        {
                            RegTmps[i] = new byte[2048];
                        }
                        byte[] paramValue = new byte[4];
                        int size = 4;
                        zkfp2.GetParameters(mDevHandle, 1, paramValue, ref size);
                        zkfp2.ByteArray2Int(paramValue, ref mfpWidth);

                        size = 4;
                       
                        zkfp2.GetParameters(mDevHandle, 2, paramValue, ref size);
                        zkfp2.ByteArray2Int(paramValue, ref mfpHeight);

                        FPBuffer = new byte[mfpWidth * mfpHeight];

                        res = true;
                    }
                    else
                    {
                        res = false;
                    }
                }
                else
                {
                    res = false;
                }

            }
            else
            {
                res = false;
            }

            return res;
        }

        public bool AdquirirHuella()
        {
            int ret = zkfp2.AcquireFingerprint(mDevHandle, FPBuffer, CapTmp, ref cbCapTmp);
            if (ret == zkfp.ZKFP_ERR_OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Bitmap getBipMap()
        {
            MemoryStream ms = new MemoryStream();
            BitmapFormat.GetBitmap(FPBuffer, mfpWidth, mfpHeight, ref ms);
            Bitmap bmp = new Bitmap(ms);
            return bmp;
        }


        //inicar hilo
        public void iniciarHilo()
        {
            Salir = false;
            if (captureThread != null)
            {
                captureThread.Resume();
            }
            else
            {
                captureThread = new Thread(new ThreadStart(DoCapture));
                captureThread.IsBackground = true;
                captureThread.Start();
                bIsTimeToDie = false;
            }
        }
        //capturar lecturas
        private void DoCapture()
        {

            while (!bIsTimeToDie)
            {
                CbCapTmp = 2048;
                if (AdquirirHuella())
                {
                    switch (accion)
                    {
                        case "identificar":
                            prueba_verificar();
                            break;
                        case "registrar":
                            prueba_registrar();
                            break;
                        default:
                            break;
                    }
                }
                Thread.Sleep(200);
            }
        }

        public void Registro()
        {
            Salir = false;
            IsRegister = false;
            accion = "registrar";
            cargarTemplates(Modelo.Templetes());
            iniciarHilo();

            if (!IsRegister)
            {
                IsRegister = true;
                MessageBox.Show("Por favor coloque su dedo en el lector 3 veces!");
            }

        }

        public void Identificar()
        {

            Salir = false;
            accion = "identificar";
            iniciarHilo();
            
            if (!bIdentify)
            {
                bIdentify = true;
            }
            MessageBox.Show("Por favor coloque su dedo en el lector!");
        }

        void prueba_verificar()
        {
            int ret = zkfp.ZKFP_ERR_OK;
            int fid = 0, score = 0;
            ret = zkfp2.DBIdentify(mDBHandle, CapTmp, ref fid, ref score);
            PictureBox.Image = getBipMap();
            if (zkfp.ZKFP_ERR_OK == ret)
            {
                IdHuellaCliente1 = fid;
            }
            else
            {
                MessageBox.Show("No se encontraron coincidencias, Intentelo de Nuevo");
                Salir = true;
                IdHuellaCliente1 = -1;
            }
            captureThread.Suspend();
        }

        void prueba_registrar()
        {
            error = 0;
            int ret = zkfp.ZKFP_ERR_OK;
            int fid = 0, score = 0;
            ret = zkfp2.DBIdentify(mDBHandle, CapTmp, ref fid, ref score);
            PictureBox.Image = getBipMap();
            if (zkfp.ZKFP_ERR_OK == ret)
            {
                //MessageBox.Show("Huella identificada usuario " + fid + "!");
                MessageBox.Show("Esta Huella Ya Se Encuentra Registrada a Otro Cliente, Intentelo con Otra Huella.");
                
            }
            else
            {

                Array.Copy(CapTmp, RegTmps[RegisterCount], cbCapTmp);
                String strBase64 = zkfp2.BlobToBase64(CapTmp, cbCapTmp);
                byte[] blob = zkfp2.Base64ToBlob(strBase64);
                RegisterCount++;
                if (RegisterCount >= REGISTER_FINGER_COUNT)
                {
                    RegisterCount = 0;
                    if (zkfp.ZKFP_ERR_OK == (ret = zkfp2.DBMerge(mDBHandle, RegTmps[0], RegTmps[1], RegTmps[2], RegTmp, ref cbRegTmp)) &&
                           zkfp.ZKFP_ERR_OK == (ret = zkfp2.DBAdd(mDBHandle, iFid, RegTmp)))
                    {
                        iFid++;
                        MessageBox.Show("Registro correcto");
                        zkfp.Blob2Base64String(RegTmp, cbRegTmp, ref templete);
                        
                        
                        //zkfp2.CloseDevice(mDevHandle);
                    }
                    else
                    {
                        error = ret;
                        //MessageBox.Show("fallo registro, error code=" + ret);
                        
                    }
                    captureThread.Suspend();
                    IsRegister = false;
                    return;
                }
                else
                {
                    if ((REGISTER_FINGER_COUNT - RegisterCount) == 1)
                    {
                        MessageBox.Show("Presiona " + (REGISTER_FINGER_COUNT - RegisterCount) + " vez mas");
                    }
                    else
                    {
                        MessageBox.Show("Presiona " + (REGISTER_FINGER_COUNT - RegisterCount) + " veces mas");
                    }

                }
            }
        }

        public static void cerrar()
        {
            zkfp2.Terminate();
            zkfp2.CloseDevice(mDevHandle);
        }

        public void cargarTemplates(Templete[] array)
        {
            if (isInitialized)
            {
                int x = zkfp2.DBClear(mDBHandle);
                if (array.Length > 0 && x==0)
                {
                    for (int i = 0; i < array.Length; i++)
                    {

                        byte[] image = Convert.FromBase64String(array[i].TempleteString);

                       int value = zkfp2.DBAdd(mDBHandle, Convert.ToInt32(array[i].Id), image);
                       
                    }
                }

            }

        }
    }
}
