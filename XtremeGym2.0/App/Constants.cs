﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XtremeGym2._0.FStack
{
    class Constants
    {
        //Cadena de conexión al torniquete. 
        public static string TORNIQUETE_CONNECTION_STRING = "protocol=TCP,ipaddress=192.168.1.201,port=4370,timeout=2000,passwd=";
        public static string URL_EVENT_TOURNIQUET_REQUEST = "https://xtremegym.com.mx/gimnasio/event/create";
        public static string URL_EVENT_ACCESS_REQUEST = "https://xtremegym.com.mx/gimnasio/access/create";
        //Dispositivo conectado correctamente.
        public static int DEVICE_CONNECTED_SUCCESS = 1;
        public static int LOG_DEFAULT = 255;
        public static int SUCCESS = 1;

        //Error desconocido
        public static int ERROR_UNKOWN = -1;

        //Control de puerta
        public const int ENTRADA = 2; //entrada
        public const int SALIDA = 1; //salida
        public static int ID_OPERATION = 1; //ref ZkTECO PULLSDK
        public static int TIME_TO_OPEN_DOOR = 1; //1 sec. con un segundo puede pasar al menos una persona.

        //Acceso a usuarios.
        public const int ACCESS_SUCCESS = 0;
        public const int ACCESS_DENIED = 23;
        public const int ACCESS_EXPIRED = 33;
        public const int ACCESS_REMOTE_OPENING = 8;
        public const int ACCESS_REMOTE_CLOSING = 9;
        public const int ACCESS_SHORT_TIME = 31;
        public const int ACCESS_UNREGISTERED = 34;
        public const int ACCESS_EMPLOYEE = 3000;
        public const int ACCESS_ABOUT_TO_EXPIRE = 3001;
        public const int ACCESS_NO_PACKAGE_REGISTERED = 3002;




        public const string UNKOWN_DOOR_STRING = "Puerta no conocida";
        public const string DOOR_IN_STRING = "Entrada";
        public const string DOOR_OUT_STRING = "Salida";



    }
}
