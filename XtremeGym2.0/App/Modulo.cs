﻿using libzkfpcsharp;
using Sample;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace XtremeGym2._0
{
    static class Modulo
    {
        public static bool isInit = false;//guarda si el lector ya esta inicializado
        public static bool isConnected = false;// guarda si se pudo conectar con el dispositivo
        public static bool isBusy = false;
        static IntPtr mDevHandle = IntPtr.Zero;//token de sesion de dispositivo
        public static string base64Template = null;// guardara la cadena del template a guardar en la base de datos

        //variables para la captura de huella
        public static bool bIsTimeToDie { get; set; } //sirve para cancelar la lectura
        public static int cbCapTmp = 2048;// in template array length, out fingerprint array length actually returned
        public static byte[] FPBuffer;//image buffer
        public static byte[] CapTmp = new byte[2048];//fingerprint template
        public static  int mfpWidth = 0;
        public static  int mfpHeight = 0;


        public static bool dbIsInit = false;//guarda si la db del lector ya esta inicializada
        public static IntPtr dbHandle=IntPtr.Zero;// guarda el handle de la db del lector

        public static int iniciar() {
            if (!isInit)
            {
                if (zkfp2.Init() == 0)
                {
                    isInit = true;
                    return 0;
                }
                else
                {
                    isInit = false;
                    return -1;
                }
            }
            else {
                isInit = true;
                return 0;
            }
        }


        public static IntPtr Conectar() {
            if (isInit)
            {
                IntPtr auxHandle = zkfp2.OpenDevice(0);
                if (auxHandle == IntPtr.Zero)
                {
                    isConnected = false;
                    return IntPtr.Zero;
                }
                else
                {
                    isConnected = true;
                    mDevHandle = auxHandle;
                    return auxHandle;
                }
            }
            else {
                isConnected = false;
                return IntPtr.Zero;
            }
            
        }

        public static void Desconectar() {

            isInit = false;
            isConnected = false;
            bIsTimeToDie = true;
            stopCaptureThread();

            if (mDevHandle == IntPtr.Zero)
            {
                return;
            }

            Thread.Sleep(1000);
            zkfp2.CloseDevice(mDevHandle);
            zkfp2.Terminate();

        }

        static private void DoCapture()
        {
            while (!bIsTimeToDie)
            {
                byte[] paramValue = new byte[4];
                int size = 4;
                zkfp2.GetParameters(mDevHandle, 1, paramValue, ref size);
                zkfp2.ByteArray2Int(paramValue, ref mfpWidth);

                size = 4;
                zkfp2.GetParameters(mDevHandle, 2, paramValue, ref size);
                zkfp2.ByteArray2Int(paramValue, ref mfpHeight);

                FPBuffer = new byte[mfpWidth * mfpHeight];
                cbCapTmp = 2048;
                int ret = zkfp2.AcquireFingerprint(mDevHandle, FPBuffer, CapTmp, ref cbCapTmp);
                if (ret == zkfp.ZKFP_ERR_OK)
                {
                    bIsTimeToDie = true;
                    isBusy = false;
                    zkfp.Blob2Base64String(CapTmp, cbCapTmp, ref base64Template);
                }
                Thread.Sleep(200);
            }
        }

        static Thread captureThread;

        static public void IniciarLectura( ) {
            captureThread = new Thread(new ThreadStart(DoCapture));
            captureThread.IsBackground = true;
            captureThread.Start();
            isBusy = true;
            bIsTimeToDie = false;
        }

        public static void stopCaptureThread()
        {
            if(captureThread!=null)
                captureThread.Abort();
        }


        public static void Reset() {
            base64Template = null;
            cbCapTmp = 2048;
            CapTmp = new byte[2048];
            mfpWidth = 0;
            mfpHeight = 0;
        }


        //db functions

        public static bool DbInit() {
            IntPtr result = zkfp2.DBInit();
            if (result == IntPtr.Zero) {
                return false;
            }
            else {
                dbIsInit = true;
                dbHandle = result;
                return true;
            }
        }

        public static bool CargarTempletes(Templete[] templates) {
            
            for (int i = 0; i < templates.Length; i++)
            {
                byte[] aux_templete = zkfp2.Base64ToBlob(templates[i].TempleteString);
                if (aux_templete!=null)
                {
                    zkfp2.DBAdd(dbHandle, int.Parse(templates[i].Id), aux_templete);
                }
                
            }

            return true;
        }

        public static bool CargarTempletesEmpleado(Templete[] templates)
        {

            for (int i = 0; i < templates.Length; i++)
            {
                byte[] aux_templete = zkfp2.Base64ToBlob(templates[i].TempleteString);
                if (aux_templete != null)
                {
                    zkfp2.DBAdd(dbHandle, int.Parse(templates[i].Id), aux_templete);
                }

            }

            return true;
        }

        public static int BuscarPorHuella() {
            int id = -1;
            int score = 0;
            int result=zkfp2.DBIdentify(dbHandle, CapTmp, ref id,ref score);

            if (result == 0) {
                return id;
            }
            else {
                return -1;
            }
        }



    }
}
