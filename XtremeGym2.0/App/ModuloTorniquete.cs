﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using XtremeGym2._0.FStack;
using XtremeGym2._0.App;
using System.Collections.Generic;

namespace XtremeGym2._0
{
    public class ModuloTorniquete
    {
        public static int FingerLeft = 1;
        public static int FingerRight = 2;

        LogDevice log = new LogDevice();

        #region variables
        IntPtr h = IntPtr.Zero;
        #endregion variables

        #region connection device

        #region DLL Import Connect
        //4.1  call connect function
        [DllImport("C:\\WINDOWS\\system32\\plcommpro.dll", EntryPoint = "Connect")]
        public static extern IntPtr Connect(string Parameters);
        [DllImport("plcommpro.dll", EntryPoint = "PullLastError")]
        public static extern int PullLastError();
        #endregion DDL Import Connect

        /**
         * Función para conectar dispositivo.
         **/
        public int connectDevice()
        {
            if (IntPtr.Zero == h)
            {
                h = Connect(Constants.TORNIQUETE_CONNECTION_STRING);
                setDeviceInicialParamsParams();
                if (h != IntPtr.Zero)
                {
                    return Constants.DEVICE_CONNECTED_SUCCESS;
                }
                else
                {
                    return PullLastError();
                }
            }

            //Algo salió mal.
            return Constants.ERROR_UNKOWN;

        }

        #region DLL Import Disconnect
        //4.2 call Disconnect function
        [DllImport("plcommpro.dll", EntryPoint = "Disconnect")]
        public static extern void Disconnect(IntPtr h);

        #endregion DLL Import Disconnect

        /**
         * Función para desconectar el dispositivo.
         */
        public void disconnectDevice()
        {
            if (IntPtr.Zero != h)
            {
                Disconnect(h);
                h = IntPtr.Zero;
            }
        }

        #endregion connection device

        #region device configuration


        //4.3 call SetDeviceParam function

        [DllImport("plcommpro.dll", EntryPoint = "SetDeviceParam")]
        public static extern int SetDeviceParam(IntPtr h, string itemvalues);

        public int setDeviceInicialParamsParams()
        {
            string str = "AntiPassback=4";
            int ret = SetDeviceParam(h, str);    //set the select param value to device
            if (ret >= 0)
                return Constants.SUCCESS;
            else
                return PullLastError();

        }
        #endregion device configuration

        #region control device

        #region DLL Import Control device
        //4.5 call ControlDevice function

        [DllImport("plcommpro.dll", EntryPoint = "ControlDevice")]
        public static extern int ControlDevice(IntPtr h, int operationid, int param1, int param2, int param3, int param4, string options);
        #endregion DLL Import Control device

        /**
         * Función para abrir y cerrar el torniquete
         */
        public int openDoorManually(int doorToOpen)
        {
            int ret = 0;
            int operID = Constants.ID_OPERATION;
            int doorOrAuxoutID = doorToOpen; //numero de puerta
            int outputAddrType = Constants.ID_OPERATION;
            int doorAction = Constants.TIME_TO_OPEN_DOOR;


            if (IntPtr.Zero != h)
            {
                ret = ControlDevice(h, operID, doorOrAuxoutID, outputAddrType, doorAction, 0, "");     //call ControlDevice funtion from PullSDK
            }
            else
            {
                return PullLastError();
            }

            if (ret >= 0)
            {
                return Constants.SUCCESS;
            }

            return Constants.ERROR_UNKOWN;

        }
        #endregion control device

        #region Log

        #region DLL Import
        //4.10 call GetRTLog function
        [DllImport("plcommpro.dll", EntryPoint = "GetRTLog")]
        public static extern int GetRTLog(IntPtr h, ref byte buffer, int buffersize);
        #endregion DLL Import

        /**
         * Obtiene el log de los eventos del torniquete.
         */
        public Dictionary<string, string> getIdByReadingLog()
        {
            int ret = 0, buffersize = 256;
            string str = "";
            string[] tmp = null;
            byte[] buffer = new byte[256];

            if (IntPtr.Zero != h)
            {

                ret = GetRTLog(h, ref buffer[0], buffersize);
                if (ret >= 0)
                {
                    str = Encoding.Default.GetString(buffer);
                    tmp = str.Split(',');

                    Dictionary<string, string> dictionary = new Dictionary<string, string>();

                    dictionary.Add("time", tmp[0]);
                    dictionary.Add("id", tmp[1]);
                    dictionary.Add("door", tmp[3]);
                    dictionary.Add("status", tmp[4]);

                    return dictionary;

                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Update User Info

        #region DLL Import

        //4.6 call SetDeviceData function

        [DllImport("plcommpro.dll", EntryPoint = "SetDeviceData")]
        public static extern int SetDeviceData(IntPtr h, string tablename, string data, string options);

        #endregion DLL Import

        public bool updateUserAuthorizeInDoor(int id)
        {
            int ret2;
            string devtablename = "userauthorize";
            string options = "";
            string data = "Pin=" + id + "\t" + "AuthorizeTimezoneId=1\tAuthorizeDoorId=3";

            if (IntPtr.Zero != h)
            {

                ret2 = SetDeviceData(h, devtablename, data, options);
                if ( ret2 >= 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool updateUserInfoInDevice(int id, string initialDate, string finalDate)
        {
            int ret;

            string data = "Pin=" + id + "\t";

            if (initialDate != null)
            {
                data += "StartTime=" + initialDate + "\t";
            }

            if (finalDate != null)
            {
                data += "EndTime=" + finalDate;
            }

            string devtablename = "user";
            string options = "";

            if (IntPtr.Zero != h)
            {

                ret = SetDeviceData(h, devtablename, data, options);

                if (ret >= 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool updateUserTemplateInDevice(int id, int finger, string template)
        {
            int ret;
            string data = "Pin=" + id + "\t" + "FingerID=" + finger + "\t" + "Template=" + template + "\t" + "Valid=1";
            string devtablename = "templatev10";
            string options = "";

            if (IntPtr.Zero != h)
            {
                ret = SetDeviceData(h, devtablename, data, options);
                if (ret >= 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;

            }
        }

        public bool updateUserInformationInBatch(List<string> idUserActiveList)
        {

            bool flagInformation = updateUserGeneralInformationInBadge(idUserActiveList);
            bool flagAccess = updateUserAutorizationInBadge(idUserActiveList);
            bool flagTemplates = updateUserTemplatesInBadge(idUserActiveList);

            return flagInformation && flagAccess && flagTemplates;
            
        }

        private bool updateUserTemplatesInBadge(List<string> idUserActiveList)
        {

            Modelo modelo = new Modelo();

            string devtablename = "templatev10";
            string options = "";

            string data = "";
            string lineBreak = "";

            for (int i = 0; i < idUserActiveList.Count; i++)
            {

                int idUser = int.Parse(idUserActiveList[i]);
                DataTable huellas = modelo.getHuellasById(idUser);

                for (int j = 0; j < huellas.Rows.Count; j++)
                {

                    data += lineBreak + "Pin=" + idUser + "\t" + "FingerID="+ int.Parse(huellas.Rows[j][4].ToString()) + "\tTemplate=" + huellas.Rows[j][2].ToString() + "\tValid=1";
                    lineBreak = "\r\n";

                }

            }

            if (IntPtr.Zero != h)
            {
                int ret = SetDeviceData(h, devtablename, data, options);

                if (ret >= 0)
                {
                    return true;
                }
                else
                {
                    log.appendToUpdateLog("Log: SetDeviceData templates");
                    return false;
                }
            }
            else
            {
                log.appendToUpdateLog("Log: IntPtr templates");
                return false;
            }

        }

        private bool updateUserAutorizationInBadge(List<string> idUserActiveList)
        {
            
            string devtablename = "userauthorize";
            string options = "";

            string data = "";
            string lineBreak = "";

            for (int i = 0; i < idUserActiveList.Count; i++)
            {

                int idUser = int.Parse(idUserActiveList[i]);
                data += lineBreak + "Pin=" + idUser + "\t" + "AuthorizeTimezoneId=1\tAuthorizeDoorId=3";
                lineBreak = "\r\n";

            }

            if (IntPtr.Zero != h)
            {
                int ret = SetDeviceData(h, devtablename, data, options);

                if (ret >= 0)
                {
                    return true;
                }
                else
                {

                    log.appendToUpdateLog("Log: SetDeviceData autorize");
                    return false;
                }
            }
            else
            {
                log.appendToUpdateLog("Log: IntPtr autorize");
                return false;
            }

        }

        private bool updateUserGeneralInformationInBadge(List<string> idUserActiveList)
        {
            string devtablename = "user";
            string options = "";

            string data = "";
            string lineBreak = "";

            for (int i = 0; i < idUserActiveList.Count; i++)
            {

                int idUser = int.Parse(idUserActiveList[i]);
                Dictionary<string, string> dictionaryDates = Modelo.getInitialAndFinalDatesForUserId(idUser);

                if (dictionaryDates != null)
                {

                    string initialDate = dictionaryDates["initialDate"];
                    string finalDate = dictionaryDates["finalDate"];
                    data += lineBreak + "Pin=" + idUser + "\t" + "StartTime=" + initialDate + "\t" + "EndTime=" + finalDate;
                    lineBreak = "\r\n";

                }

            }

            if (IntPtr.Zero != h)
            {
                int ret = SetDeviceData(h, devtablename, data, options);

                if (ret >= 0)
                {
                    return true;
                }
                else
                {

                    log.appendToUpdateLog("Log: SetDeviceData GeneralInfo");
                    return false;
                }
            }
            else
            {

                log.appendToUpdateLog("Log: SetDeviceData GeneralInfo");
                return false;
            }
               
        }

        #endregion Update User Info


        /**
         * Obtiene el log,door de una forma legible para el grid view. 
         */
            public static Dictionary<string, string> getLogHumanReadableFormat(int log, int door)
        {

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("door", getDoorNameByNumber(door));
            dictionary.Add("status", getStatusByIntToString(log));
            return dictionary;

        }

        /**
         * Obtiene la puerta de una forma legible para el grid view. 
         */
        public static string getDoorNameByNumber(int doorNumber)
        {
            switch (doorNumber)
            {
                case Constants.ENTRADA:
                    return Constants.DOOR_IN_STRING;
                case Constants.SALIDA:
                    return Constants.DOOR_OUT_STRING;
                default:
                    return Constants.UNKOWN_DOOR_STRING;
            }
        }
        /**
         * Obtiene el status id de una forma legible para el grid view. 
         * Ver documentaicón de pull sdk
         */
        public static string getStatusByIntToString(int log)
        {
            switch (log)
            {
                case Constants.ACCESS_SUCCESS:
                    return "Vigente";
                case Constants.ACCESS_DENIED:
                    return "Denegado";
                case Constants.ACCESS_EXPIRED:
                    return "Expirado";
                case Constants.ACCESS_REMOTE_OPENING:
                    return "Apertura manual";
                case Constants.ACCESS_REMOTE_CLOSING:
                    return "Cierre manual";
                case Constants.ACCESS_SHORT_TIME:
                    return "Corto Tiempo";
                case Constants.ACCESS_UNREGISTERED:
                    return "Huella no registrada";
                case Constants.ACCESS_EMPLOYEE:
                    return "Empleado";
                case Constants.ACCESS_ABOUT_TO_EXPIRE:
                    return "Acceso próximo a vencer";
                case Constants.ACCESS_NO_PACKAGE_REGISTERED:
                    return "-";
                default:
                    return "No conocido";
            }

        }

    }
}
