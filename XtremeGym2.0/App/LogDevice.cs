﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XtremeGym2._0.App
{
    class LogDevice
    {

        internal static int LOG_ACCESS = 0;
        internal static int LOG_EVENT = 1;

        string fileLogName = @"log_file.txt";
        string fileNameUpdate = @"log_file_update.txt";
        string folderName = @"" + AppDomain.CurrentDomain.BaseDirectory;

        internal void appendToUpdateLog(string lineIdAndName) {

            writeToFile(lineIdAndName, fileNameUpdate);

        }


        /**
         * Guarda un evento de apertura manual.
         */
        internal void appendToLog(int logId, int reason, int type, string date, string time, string description)
        {

            string line = logId + ";" + reason + ";" + type + ";" + date + ";" + time + ";" + description;
            writeToFile(line, fileLogName);

        }

        /**
         * Guarda un evento de acceso de usuario.
         */
        internal void appendToLog(int logId, int idUser, int door, bool flagStatus, string date, string time)
        {

            string line = logId + ";" + idUser + ";" + door + ";" + flagStatus + ";" + date + ";" + time;
            writeToFile(line, fileLogName);

        }

        /**
         * Escribe una linea de log
         */
        private bool writeToFile(string logLine, string fileName)
        {
            try
            {

                string pathString = System.IO.Path.Combine(folderName, fileName);

                if (!System.IO.File.Exists(pathString))
                {

                    System.IO.FileStream fs = System.IO.File.Create(pathString);
                    fs.Close();

                }

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(pathString, true))
                {
                    file.WriteLine(logLine);
                    file.Close();
                }

                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        /**
         * Lee el log
         */
        internal List<string> getLogEvents()
        {
            try
            {

                string pathString = System.IO.Path.Combine(folderName, fileLogName);

                if (!System.IO.File.Exists(pathString))
                {

                    return null;

                }

                return new List<string>(System.IO.File.ReadAllLines(pathString));

            }
            catch (Exception)
            {
                return null;
            }
        }


        /**
         * Limpia el log
         */
        internal void clearContentOfLog()
        {
            try
            {

                string pathString = System.IO.Path.Combine(folderName, fileLogName);

                if (!System.IO.File.Exists(pathString))
                {

                    return;

                }

                System.IO.File.WriteAllText(pathString, string.Empty);

            }
            catch (Exception)
            {
                return;
            }
        }

        internal void rewriteArrayOfLogs(List<string> list)
        {

            clearContentOfLog();

            foreach (string line in list)
            {
                writeToFile(line, fileLogName);
            }

        }

    }
}
