﻿namespace XtremeGym2._0
{
    partial class Detalle_UC
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lbl_registro;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label15;
            this.TXT_DETALLE = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_Correo = new System.Windows.Forms.TextBox();
            this.radio_sf = new System.Windows.Forms.RadioButton();
            this.radio_sm = new System.Windows.Forms.RadioButton();
            this.txt_edad = new System.Windows.Forms.TextBox();
            this.txt_ApellidoMaterno = new System.Windows.Forms.TextBox();
            this.txt_Nombre = new System.Windows.Forms.TextBox();
            this.txt_ApellidoPaterno = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel_Direccion = new System.Windows.Forms.GroupBox();
            this.txt_Telefono = new System.Windows.Forms.TextBox();
            this.txt_CP = new System.Windows.Forms.TextBox();
            this.txt_Ciudad = new System.Windows.Forms.TextBox();
            this.txt_Colonia = new System.Windows.Forms.TextBox();
            this.txt_Calle = new System.Windows.Forms.TextBox();
            this.txt_Numero = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            lbl_registro = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label15 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel_Direccion.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_registro
            // 
            lbl_registro.AutoSize = true;
            lbl_registro.BackColor = System.Drawing.Color.Transparent;
            lbl_registro.Font = new System.Drawing.Font("Century Gothic", 25F);
            lbl_registro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            lbl_registro.Location = new System.Drawing.Point(3, 10);
            lbl_registro.Name = "lbl_registro";
            lbl_registro.Size = new System.Drawing.Size(326, 40);
            lbl_registro.TabIndex = 21;
            lbl_registro.Text = "Detalles de Usuario";
            lbl_registro.Click += new System.EventHandler(this.lbl_registro_Click);
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = System.Drawing.Color.Transparent;
            label1.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label1.Location = new System.Drawing.Point(335, 10);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(85, 19);
            label1.TabIndex = 17;
            label1.Text = "No. Usuario";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = System.Drawing.Color.Transparent;
            label2.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label2.Location = new System.Drawing.Point(10, 82);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(55, 19);
            label2.TabIndex = 15;
            label2.Text = "Correo";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.BackColor = System.Drawing.Color.Transparent;
            label12.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label12.Location = new System.Drawing.Point(615, 39);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(41, 19);
            label12.TabIndex = 10;
            label12.Text = "Sexo";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.BackColor = System.Drawing.Color.Transparent;
            label5.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label5.Location = new System.Drawing.Point(518, 39);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(47, 19);
            label5.TabIndex = 8;
            label5.Text = "Edad";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.BackColor = System.Drawing.Color.Transparent;
            label4.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label4.Location = new System.Drawing.Point(336, 28);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(128, 19);
            label4.TabIndex = 6;
            label4.Text = "Apellido Materno";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = System.Drawing.Color.Transparent;
            label3.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label3.Location = new System.Drawing.Point(10, 28);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(79, 19);
            label3.TabIndex = 1;
            label3.Text = "Nombre(s)";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.BackColor = System.Drawing.Color.Transparent;
            label13.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label13.Location = new System.Drawing.Point(196, 28);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(123, 19);
            label13.TabIndex = 4;
            label13.Text = "Apellido Paterno";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.BackColor = System.Drawing.Color.Transparent;
            label11.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label11.Location = new System.Drawing.Point(571, 28);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(66, 19);
            label11.TabIndex = 14;
            label11.Text = "Telefono";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.BackColor = System.Drawing.Color.Transparent;
            label10.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label10.Location = new System.Drawing.Point(511, 28);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(36, 19);
            label10.TabIndex = 10;
            label10.Text = "C.P.";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.BackColor = System.Drawing.Color.Transparent;
            label7.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label7.Location = new System.Drawing.Point(381, 28);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(62, 19);
            label7.TabIndex = 8;
            label7.Text = "Ciudad";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.BackColor = System.Drawing.Color.Transparent;
            label8.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label8.Location = new System.Drawing.Point(238, 28);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(63, 19);
            label8.TabIndex = 6;
            label8.Text = "Colonia";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.BackColor = System.Drawing.Color.Transparent;
            label9.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label9.Location = new System.Drawing.Point(10, 28);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(45, 19);
            label9.TabIndex = 1;
            label9.Text = "Calle";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.BackColor = System.Drawing.Color.Transparent;
            label14.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label14.Location = new System.Drawing.Point(169, 28);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(63, 19);
            label14.TabIndex = 4;
            label14.Text = "Numero";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.BackColor = System.Drawing.Color.Transparent;
            label6.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label6.Location = new System.Drawing.Point(221, 16);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(82, 19);
            label6.TabIndex = 17;
            label6.Text = "Dedo. Der.";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.BackColor = System.Drawing.Color.Transparent;
            label15.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label15.Location = new System.Drawing.Point(141, 16);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(78, 19);
            label15.TabIndex = 16;
            label15.Text = "Dedo. Izq.";
            // 
            // TXT_DETALLE
            // 
            this.TXT_DETALLE.BackColor = System.Drawing.Color.LightGray;
            this.TXT_DETALLE.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.TXT_DETALLE.ForeColor = System.Drawing.Color.Black;
            this.TXT_DETALLE.Location = new System.Drawing.Point(339, 32);
            this.TXT_DETALLE.Name = "TXT_DETALLE";
            this.TXT_DETALLE.Size = new System.Drawing.Size(81, 25);
            this.TXT_DETALLE.TabIndex = 17;
            this.TXT_DETALLE.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox1.CausesValidation = false;
            this.groupBox1.Controls.Add(label2);
            this.groupBox1.Controls.Add(this.txt_Correo);
            this.groupBox1.Controls.Add(this.radio_sf);
            this.groupBox1.Controls.Add(this.radio_sm);
            this.groupBox1.Controls.Add(label12);
            this.groupBox1.Controls.Add(this.txt_edad);
            this.groupBox1.Controls.Add(label5);
            this.groupBox1.Controls.Add(label4);
            this.groupBox1.Controls.Add(this.txt_ApellidoMaterno);
            this.groupBox1.Controls.Add(label3);
            this.groupBox1.Controls.Add(this.txt_Nombre);
            this.groupBox1.Controls.Add(this.txt_ApellidoPaterno);
            this.groupBox1.Controls.Add(label13);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.groupBox1.Location = new System.Drawing.Point(10, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(701, 117);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Personales";
            // 
            // txt_Correo
            // 
            this.txt_Correo.BackColor = System.Drawing.Color.LightGray;
            this.txt_Correo.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_Correo.ForeColor = System.Drawing.Color.Black;
            this.txt_Correo.Location = new System.Drawing.Point(71, 81);
            this.txt_Correo.Name = "txt_Correo";
            this.txt_Correo.Size = new System.Drawing.Size(400, 25);
            this.txt_Correo.TabIndex = 16;
            // 
            // radio_sf
            // 
            this.radio_sf.AutoSize = true;
            this.radio_sf.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.radio_sf.Location = new System.Drawing.Point(641, 59);
            this.radio_sf.Name = "radio_sf";
            this.radio_sf.Size = new System.Drawing.Size(34, 23);
            this.radio_sf.TabIndex = 14;
            this.radio_sf.TabStop = true;
            this.radio_sf.Text = "F";
            this.radio_sf.UseVisualStyleBackColor = true;
            // 
            // radio_sm
            // 
            this.radio_sm.AutoSize = true;
            this.radio_sm.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.radio_sm.Location = new System.Drawing.Point(595, 59);
            this.radio_sm.Name = "radio_sm";
            this.radio_sm.Size = new System.Drawing.Size(40, 23);
            this.radio_sm.TabIndex = 11;
            this.radio_sm.TabStop = true;
            this.radio_sm.Text = "M";
            this.radio_sm.UseVisualStyleBackColor = true;
            // 
            // txt_edad
            // 
            this.txt_edad.BackColor = System.Drawing.Color.LightGray;
            this.txt_edad.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_edad.ForeColor = System.Drawing.Color.Black;
            this.txt_edad.Location = new System.Drawing.Point(522, 61);
            this.txt_edad.Name = "txt_edad";
            this.txt_edad.Size = new System.Drawing.Size(43, 25);
            this.txt_edad.TabIndex = 9;
            // 
            // txt_ApellidoMaterno
            // 
            this.txt_ApellidoMaterno.BackColor = System.Drawing.Color.LightGray;
            this.txt_ApellidoMaterno.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_ApellidoMaterno.ForeColor = System.Drawing.Color.Black;
            this.txt_ApellidoMaterno.Location = new System.Drawing.Point(340, 50);
            this.txt_ApellidoMaterno.Name = "txt_ApellidoMaterno";
            this.txt_ApellidoMaterno.Size = new System.Drawing.Size(131, 25);
            this.txt_ApellidoMaterno.TabIndex = 7;
            // 
            // txt_Nombre
            // 
            this.txt_Nombre.BackColor = System.Drawing.Color.LightGray;
            this.txt_Nombre.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_Nombre.ForeColor = System.Drawing.Color.Black;
            this.txt_Nombre.Location = new System.Drawing.Point(14, 50);
            this.txt_Nombre.Name = "txt_Nombre";
            this.txt_Nombre.Size = new System.Drawing.Size(172, 25);
            this.txt_Nombre.TabIndex = 3;
            // 
            // txt_ApellidoPaterno
            // 
            this.txt_ApellidoPaterno.BackColor = System.Drawing.Color.LightGray;
            this.txt_ApellidoPaterno.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_ApellidoPaterno.ForeColor = System.Drawing.Color.Black;
            this.txt_ApellidoPaterno.Location = new System.Drawing.Point(192, 50);
            this.txt_ApellidoPaterno.Name = "txt_ApellidoPaterno";
            this.txt_ApellidoPaterno.Size = new System.Drawing.Size(141, 25);
            this.txt_ApellidoPaterno.TabIndex = 5;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox4.CausesValidation = false;
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.pictureBox2);
            this.groupBox4.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.groupBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.groupBox4.Location = new System.Drawing.Point(10, 276);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(392, 115);
            this.groupBox4.TabIndex = 26;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = " Fotografia";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.button5.Location = new System.Drawing.Point(14, 31);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(260, 74);
            this.button5.TabIndex = 10;
            this.button5.Text = "Cambiar y Tomar Fotografia desde Camara";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(280, 16);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(106, 89);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // panel_Direccion
            // 
            this.panel_Direccion.BackColor = System.Drawing.Color.Transparent;
            this.panel_Direccion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel_Direccion.CausesValidation = false;
            this.panel_Direccion.Controls.Add(this.txt_Telefono);
            this.panel_Direccion.Controls.Add(label11);
            this.panel_Direccion.Controls.Add(this.txt_CP);
            this.panel_Direccion.Controls.Add(label10);
            this.panel_Direccion.Controls.Add(this.txt_Ciudad);
            this.panel_Direccion.Controls.Add(label7);
            this.panel_Direccion.Controls.Add(label8);
            this.panel_Direccion.Controls.Add(this.txt_Colonia);
            this.panel_Direccion.Controls.Add(label9);
            this.panel_Direccion.Controls.Add(this.txt_Calle);
            this.panel_Direccion.Controls.Add(this.txt_Numero);
            this.panel_Direccion.Controls.Add(label14);
            this.panel_Direccion.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.panel_Direccion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.panel_Direccion.Location = new System.Drawing.Point(10, 176);
            this.panel_Direccion.Name = "panel_Direccion";
            this.panel_Direccion.Size = new System.Drawing.Size(701, 94);
            this.panel_Direccion.TabIndex = 25;
            this.panel_Direccion.TabStop = false;
            this.panel_Direccion.Text = "Dirección";
            // 
            // txt_Telefono
            // 
            this.txt_Telefono.BackColor = System.Drawing.Color.LightGray;
            this.txt_Telefono.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_Telefono.ForeColor = System.Drawing.Color.Black;
            this.txt_Telefono.Location = new System.Drawing.Point(575, 50);
            this.txt_Telefono.Name = "txt_Telefono";
            this.txt_Telefono.Size = new System.Drawing.Size(120, 25);
            this.txt_Telefono.TabIndex = 15;
            // 
            // txt_CP
            // 
            this.txt_CP.BackColor = System.Drawing.Color.LightGray;
            this.txt_CP.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_CP.ForeColor = System.Drawing.Color.Black;
            this.txt_CP.Location = new System.Drawing.Point(515, 50);
            this.txt_CP.Name = "txt_CP";
            this.txt_CP.Size = new System.Drawing.Size(54, 25);
            this.txt_CP.TabIndex = 11;
            // 
            // txt_Ciudad
            // 
            this.txt_Ciudad.BackColor = System.Drawing.Color.LightGray;
            this.txt_Ciudad.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_Ciudad.ForeColor = System.Drawing.Color.Black;
            this.txt_Ciudad.Location = new System.Drawing.Point(385, 50);
            this.txt_Ciudad.Name = "txt_Ciudad";
            this.txt_Ciudad.Size = new System.Drawing.Size(124, 25);
            this.txt_Ciudad.TabIndex = 9;
            // 
            // txt_Colonia
            // 
            this.txt_Colonia.BackColor = System.Drawing.Color.LightGray;
            this.txt_Colonia.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_Colonia.ForeColor = System.Drawing.Color.Black;
            this.txt_Colonia.Location = new System.Drawing.Point(238, 50);
            this.txt_Colonia.Name = "txt_Colonia";
            this.txt_Colonia.Size = new System.Drawing.Size(141, 25);
            this.txt_Colonia.TabIndex = 7;
            // 
            // txt_Calle
            // 
            this.txt_Calle.BackColor = System.Drawing.Color.LightGray;
            this.txt_Calle.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_Calle.ForeColor = System.Drawing.Color.Black;
            this.txt_Calle.Location = new System.Drawing.Point(14, 50);
            this.txt_Calle.Name = "txt_Calle";
            this.txt_Calle.Size = new System.Drawing.Size(153, 25);
            this.txt_Calle.TabIndex = 3;
            // 
            // txt_Numero
            // 
            this.txt_Numero.BackColor = System.Drawing.Color.LightGray;
            this.txt_Numero.Font = new System.Drawing.Font("Century Gothic", 10.5F);
            this.txt_Numero.ForeColor = System.Drawing.Color.Black;
            this.txt_Numero.Location = new System.Drawing.Point(173, 50);
            this.txt_Numero.Name = "txt_Numero";
            this.txt_Numero.Size = new System.Drawing.Size(59, 25);
            this.txt_Numero.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox2.CausesValidation = false;
            this.groupBox2.Controls.Add(label6);
            this.groupBox2.Controls.Add(label15);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.groupBox2.Location = new System.Drawing.Point(408, 276);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(303, 115);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Huella Digital";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.button1.Location = new System.Drawing.Point(11, 69);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 36);
            this.button1.TabIndex = 7;
            this.button1.Text = "Scanear Der.";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox3.Location = new System.Drawing.Point(228, 38);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(62, 67);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.button3.Location = new System.Drawing.Point(11, 30);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(110, 36);
            this.button3.TabIndex = 5;
            this.button3.Text = "Scanear Izq.";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(148, 38);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 67);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.groupBox3.Location = new System.Drawing.Point(717, 53);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(133, 338);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Finalizar";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.button4.Location = new System.Drawing.Point(8, 28);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(121, 58);
            this.button4.TabIndex = 20;
            this.button4.Text = " Cancelar Registro";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.button2.Location = new System.Drawing.Point(6, 173);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(123, 155);
            this.button2.TabIndex = 4;
            this.button2.Text = "Terminar Registro";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Detalle_UC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.panel_Direccion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(label1);
            this.Controls.Add(this.TXT_DETALLE);
            this.Controls.Add(lbl_registro);
            this.Name = "Detalle_UC";
            this.Size = new System.Drawing.Size(862, 408);
            this.Load += new System.EventHandler(this.Detalle_UC_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel_Direccion.ResumeLayout(false);
            this.panel_Direccion.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox TXT_DETALLE;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox txt_Correo;
        public System.Windows.Forms.RadioButton radio_sf;
        public System.Windows.Forms.RadioButton radio_sm;
        public System.Windows.Forms.TextBox txt_edad;
        public System.Windows.Forms.TextBox txt_Nombre;
        public System.Windows.Forms.TextBox txt_ApellidoPaterno;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox panel_Direccion;
        public System.Windows.Forms.TextBox txt_Telefono;
        public System.Windows.Forms.TextBox txt_Ciudad;
        public System.Windows.Forms.TextBox txt_Colonia;
        public System.Windows.Forms.TextBox txt_Calle;
        public System.Windows.Forms.TextBox txt_Numero;
        public System.Windows.Forms.TextBox txt_ApellidoMaterno;
        public System.Windows.Forms.TextBox txt_CP;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.PictureBox pictureBox3;
    }
}
