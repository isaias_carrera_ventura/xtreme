﻿
using System.Windows.Forms;
using System;

namespace XtremeGym2._0
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// this.registro_UC1 = new XtremeGym2._0.Registro_UC(panel1,btn_Home,home_UC1);}
        /// this.registro_UC1 = new XtremeGym2._0.Registro_UC(panel1, btn_Home, home_UC1, panel_Status, lbl_Status);
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        //private Usuarios usuarios_UC1;

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel_Status = new System.Windows.Forms.Panel();
            this.lbl_Status = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.usuarios_UC1 = new XtremeGym2._0.Usuarios();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.home_UC1 = new XtremeGym2._0.Home_UC();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.empleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuToolbox = new System.Windows.Forms.ToolStripMenuItem();
            this.conectToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.disconectToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.detalle_UC1 = new XtremeGym2._0.Detalle_UC();
            this.accesoUsuario1 = new XtremeGym2._0.AccesoUsuario();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel_Status.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Status
            // 
            this.panel_Status.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel_Status.Controls.Add(this.lbl_Status);
            this.panel_Status.Controls.Add(this.label1);
            this.panel_Status.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_Status.Location = new System.Drawing.Point(0, 551);
            this.panel_Status.Name = "panel_Status";
            this.panel_Status.Size = new System.Drawing.Size(815, 28);
            this.panel_Status.TabIndex = 7;
            // 
            // lbl_Status
            // 
            this.lbl_Status.AutoSize = true;
            this.lbl_Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lbl_Status.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_Status.Location = new System.Drawing.Point(116, 5);
            this.lbl_Status.Name = "lbl_Status";
            this.lbl_Status.Size = new System.Drawing.Size(111, 18);
            this.lbl_Status.TabIndex = 1;
            this.lbl_Status.Text = "Sin Acciones.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.75F);
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Status Actual:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.usuarios_UC1);
            this.tabPage3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(809, 311);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Usuarios";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // usuarios_UC1
            // 
            this.usuarios_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.usuarios_UC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usuarios_UC1.Lbl_status = null;
            this.usuarios_UC1.Location = new System.Drawing.Point(0, 0);
            this.usuarios_UC1.Margin = new System.Windows.Forms.Padding(6);
            this.usuarios_UC1.Name = "usuarios_UC1";
            this.usuarios_UC1.Size = new System.Drawing.Size(809, 311);
            this.usuarios_UC1.TabIndex = 9;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.home_UC1);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(809, 311);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Control de Puertas";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // home_UC1
            // 
            this.home_UC1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.home_UC1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home_UC1.Lbl_status = null;
            this.home_UC1.Location = new System.Drawing.Point(3, 3);
            this.home_UC1.Margin = new System.Windows.Forms.Padding(6);
            this.home_UC1.Name = "home_UC1";
            this.home_UC1.Size = new System.Drawing.Size(803, 305);
            this.home_UC1.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 50);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(817, 340);
            this.tabControl1.TabIndex = 1005;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.empleadoToolStripMenuItem,
            this.menuToolbox});
            this.menuStrip1.Location = new System.Drawing.Point(12, 9);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(263, 29);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // empleadoToolStripMenuItem
            // 
            this.empleadoToolStripMenuItem.Name = "empleadoToolStripMenuItem";
            this.empleadoToolStripMenuItem.Size = new System.Drawing.Size(91, 25);
            this.empleadoToolStripMenuItem.Text = "Empleado";
            this.empleadoToolStripMenuItem.Click += new System.EventHandler(this.EmpleadoToolStripMenuItem_Click);
            // 
            // menuToolbox
            // 
            this.menuToolbox.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.conectToolStrip,
            this.disconectToolStrip});
            this.menuToolbox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuToolbox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.menuToolbox.Name = "menuToolbox";
            this.menuToolbox.Size = new System.Drawing.Size(164, 25);
            this.menuToolbox.Text = "Opciones Torniquete";
            // 
            // conectToolStrip
            // 
            this.conectToolStrip.Name = "conectToolStrip";
            this.conectToolStrip.Size = new System.Drawing.Size(241, 26);
            this.conectToolStrip.Text = "Conectar torniquete";
            this.conectToolStrip.Click += new System.EventHandler(this.ToolStripMenuItem3_Click);
            // 
            // disconectToolStrip
            // 
            this.disconectToolStrip.Name = "disconectToolStrip";
            this.disconectToolStrip.Size = new System.Drawing.Size(241, 26);
            this.disconectToolStrip.Text = "Desconectar torniquete";
            this.disconectToolStrip.Click += new System.EventHandler(this.DesconectarToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.button1.ForeColor = System.Drawing.Color.DarkGray;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(1101, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 29);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.button2.ForeColor = System.Drawing.Color.DarkGray;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(1066, 1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(29, 29);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.menuStrip1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(815, 44);
            this.panel2.TabIndex = 1;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.button3.ForeColor = System.Drawing.Color.DarkGray;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(741, 9);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(29, 29);
            this.button3.TabIndex = 4;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.button4.ForeColor = System.Drawing.Color.DarkGray;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(776, 9);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(29, 29);
            this.button4.TabIndex = 3;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // detalle_UC1
            // 
            this.detalle_UC1.Btn = null;
            this.detalle_UC1.Fp = null;
            this.detalle_UC1.Home = null;
            this.detalle_UC1.Id_cliente = 0;
            this.detalle_UC1.Idtemplate1 = 0;
            this.detalle_UC1.Idtemplate2 = 0;
            this.detalle_UC1.Lbl_status = null;
            this.detalle_UC1.Location = new System.Drawing.Point(176, 37);
            this.detalle_UC1.Mimodelo = null;
            this.detalle_UC1.Name = "detalle_UC1";
            this.detalle_UC1.P = null;
            this.detalle_UC1.Size = new System.Drawing.Size(862, 417);
            this.detalle_UC1.Status = null;
            this.detalle_UC1.TabIndex = 2;
            this.detalle_UC1.TempleteString = null;
            this.detalle_UC1.TempleteString2 = null;
            // 
            // accesoUsuario1
            // 
            this.accesoUsuario1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accesoUsuario1.Location = new System.Drawing.Point(3, 22);
            this.accesoUsuario1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.accesoUsuario1.Name = "accesoUsuario1";
            this.accesoUsuario1.Size = new System.Drawing.Size(809, 130);
            this.accesoUsuario1.TabIndex = 1006;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.accesoUsuario1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.groupBox1.Location = new System.Drawing.Point(0, 396);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(815, 155);
            this.groupBox1.TabIndex = 1007;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Acceso usuarios:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(815, 579);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel_Status);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel_Status.ResumeLayout(false);
            this.panel_Status.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void usuarios_UC1_Load_1(object sender, EventArgs e)
        {
            usuarios_UC1.dataGridView1.DataSource = modelo.getUsuarios();
            usuarios_UC1.dataGridView1.Columns["IdCliente"].Visible = false;
            usuarios_UC1.dataGridView1.AutoResizeColumns();
        }


        #endregion
        private System.Windows.Forms.Panel panel_Status;
        private System.Windows.Forms.Label lbl_Status;
        private System.Windows.Forms.Label label1;
        private Detalle_UC detalle_UC1;
        private TabPage tabPage3;
        private Usuarios usuarios_UC1;
        private TabPage tabPage2;
        private Home_UC home_UC1;
        private TabControl tabControl1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem menuToolbox;
        private ToolStripMenuItem conectToolStrip;
        private ToolStripMenuItem disconectToolStrip;
        private Button button1;
        private Button button2;
        private Panel panel2;
        private Button button3;
        private Button button4;
        private ToolStripMenuItem empleadoToolStripMenuItem;
        private AccesoUsuario accesoUsuario1;
        private GroupBox groupBox1;
    }
}

