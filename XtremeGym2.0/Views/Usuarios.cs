﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using XtremeGym2._0.FStack;
using XtremeGym2._0.Views;

namespace XtremeGym2._0
{
    public partial class Usuarios : UserControl
    {

        Modelo modelo = new Modelo();
        UsuarioActualiza detalle;
        private Label lbl_status;
        TypeAssistant assistant;
        private ModuloTorniquete moduloTorniquete;

        public Label Lbl_status { get => lbl_status; set => lbl_status = value; }

        public Usuarios()
        {

            InitializeComponent();

            assistant = new TypeAssistant();
            assistant.Idled += assistant_Idled;

        }

        internal void setModuloTorniquete(ModuloTorniquete modulo)
        {
            moduloTorniquete = modulo;
        }

        void assistant_Idled(object sender, EventArgs e)
        {
            try
            {

                this.Invoke(
                new MethodInvoker(() =>
                {
                    if (!String.IsNullOrEmpty(txt_FiltroNombre.Text) && !String.IsNullOrWhiteSpace(txt_FiltroNombre.Text))
                    {
                        dataGridView1.DataSource = modelo.getUsuariosByName(txt_FiltroNombre.Text);
                    }
                    else
                    {
                        dataGridView1.DataSource = modelo.getUsuarios();
                    }
                }));
            }
            catch (Exception)
            {
                this.Invoke(
                new MethodInvoker(() =>
                {
                    lbl_status.Text = "Error: Sin conexión a internet.";
                }));
            }
        }


        private void txt_FiltroNombre_TextChanged(object sender, EventArgs e)
        {
            assistant.TextChanged();    
        }

        private void Usuarios_Load(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            lbl_status.Text="Cargando...";

            Thread thread = new Thread(() => {

                DataGridViewRow selectedRow = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex];
                detalle = new UsuarioActualiza(int.Parse(selectedRow.Cells["IdCliente"].Value.ToString()),moduloTorniquete);
                this.BeginInvoke(new Action(labelText));

            });

            thread.Start();
        }



        void labelText() {

            lbl_status.Text = "Sin acciones";
            detalle.ShowDialog();

        }

    }
}
