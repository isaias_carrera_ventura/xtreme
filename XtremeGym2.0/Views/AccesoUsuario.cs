﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using XtremeGym2._0.FStack;
using XtremeGym2._0.App;

namespace XtremeGym2._0
{
    public partial class AccesoUsuario : UserControl
    {

        private ModuloTorniquete moduloTorniquete;
        private EventDatabaseRequest eventRequest = new EventDatabaseRequest();
        private Modelo modelo = new Modelo();
        UsuarioActualiza detalle;
        private Label lbl_status;

        public Label Lbl_status { get => lbl_status; set => lbl_status = value; }

        public AccesoUsuario()
        {
            InitializeComponent();
        }

        private void AccesoUsuario_Load(object sender, EventArgs e)
        {
        }

        internal void stopReadLogInTimer()
        {

            this.moduloTorniquete = null;
            timerLog.Enabled = false;

        }

        /**
         * Inicializa el timer y empieza a leer el log. 
         */
        internal void readLogInTimer(ModuloTorniquete moduloTorniquete)
        {

            this.moduloTorniquete = moduloTorniquete;
            timerLog.Enabled = true;

        }

        /**
         * Timer tick
         * Obtenemos la información del torniquete 
         * y mandamos a llamar a API.
         */
        private void TimerLog_Tick(object sender, EventArgs e)
        {
             if (moduloTorniquete != null)
            {

                Dictionary<string, string> log = moduloTorniquete.getIdByReadingLog();

                if (log != null)
                {

                    int logNumber = int.Parse(log["status"]);

                    if (logNumber != Constants.LOG_DEFAULT)
                    {

                        int door = int.Parse(log["door"]);
                        int id = int.Parse(log["id"]);
                        string time = log["time"];

                        switch (logNumber)
                        {

                            case Constants.ACCESS_SUCCESS:
                                registerAccessInServer(id, door, time, true);
                                break;

                            case Constants.ACCESS_EXPIRED:
                                registerAccessInServer(id, door, time, false);
                                break;

                            default:
                                registerLogEventInDevice(0, "-", time, logNumber, door);
                                break;
                        }

                    }
                }
            }
        }

        /**
         * Realiza una llamada al server
         */
        private void registerAccessInServer(int id, int door, string time, bool access)
        {

            Thread thread = new Thread(() =>
            {
                //Si es empleado, registramos su salida/entrada
                //Si no mandamos la asistencia, ya que fue cliente.

                int status;
                string name;

                if (modelo.isUserEmployee(id))
                {
                    status = Constants.ACCESS_EMPLOYEE;
                    modelo.setScheduleForEmployee(id);

                }
                else
                {

                    status = modelo.getUserValidityAsStatus(id); 
                    eventRequest.sendAccessEvent(id, door, access);

                }

                try
                {

                    DataTable tabla = modelo.getUsuarioById(id);
                    name = tabla.Rows[0][3].ToString() + " " + tabla.Rows[0][4].ToString();

                }catch (Exception e){
                    name = "-";
                }

                this.BeginInvoke(
                            (Action<string>)((description) =>
                            {
                                registerLogEventInDevice(id, name, time, status, door);
                            }),
                            new object[] { "" }
                          );

            });

            thread.Start();
        }

        /**
         * Imprime un mensaje en el grid view.
         */
        private void registerLogEventInDevice(int id, string name, string hour, int status, int door)
        {
            Dictionary<string, string> values = ModuloTorniquete.getLogHumanReadableFormat(status, door);
            dataGridViewLog.Rows.Add(id, name, hour, values["status"], values["door"]);
            dataGridViewLog.FirstDisplayedScrollingRowIndex = dataGridViewLog.RowCount - 1;
        }

        private void DataGridViewLog_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (dataGridViewLog.Columns[e.ColumnIndex].Name.Equals("status"))
            {

                string stringValue = e.Value as string;
                if (stringValue == null) return;

                DataGridViewImageCell cell = (DataGridViewImageCell)dataGridViewLog[5, e.RowIndex];
                cell.ToolTipText = stringValue;

                switch (stringValue)
                {
                    case "Vigente":
                        cell.Value = Properties.Resources.WX_circle_green;
                        break;
                    case "Denegado":
                        cell.Value = Properties.Resources.WX_circle_red;
                        break;
                    case "Expirado":
                        cell.Value = Properties.Resources.WX_circle_red;
                        break;
                    case "Acceso próximo a vencer":
                        cell.Value = Properties.Resources.WX_circle_yellow;
                        break;
                    default:
                        cell.Value = null;
                        break;
                }

            }

        }

        private void DataGridViewLog_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            lbl_status.Text = "Cargando...";
           
            Thread thread = new Thread(() => {

                DataGridViewRow selectedRow = dataGridViewLog.Rows[dataGridViewLog.SelectedCells[0].RowIndex];
                detalle = new UsuarioActualiza(int.Parse(selectedRow.Cells["idUser"].Value.ToString()), moduloTorniquete);
                this.BeginInvoke(new Action(labelText));
                

            });

            thread.Start();
        }

        void labelText()
        {
            lbl_status.Text = "Sin acciones";
            detalle.ShowDialog();
        }

    }
}
