﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XtremeGym2._0.Views
{
    public partial class formWait : Form
    {

        public Action Worker { get; set; }
        public formWait(Action worker)
        {

            InitializeComponent();
            Worker = worker ?? throw new ArgumentNullException();

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Task.Factory.StartNew(Worker).ContinueWith(t =>
            {
                this.Close();
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

    }
}
