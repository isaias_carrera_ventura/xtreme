﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using AForge.Video;
using AForge.Video.DirectShow;
using XtremeGym2._0.Servicios;

namespace XtremeGym2._0
{
    public partial class CapturaFoto : Form
    {
        private bool ExisteDispositivo = false;
        private FilterInfoCollection DispositivoDeVideo;
        private VideoCaptureDevice FuenteDeVideo = null;
        PictureBox pb;
        int IdUsuario;
        Panel status;
        Label lbl_status;

        public CapturaFoto(PictureBox pb , Panel status, Label lbl,int id)
        {
            InitializeComponent();
            BuscarDispositivos();
            this.pb = pb;
            this.status = status;
            this.lbl_status = lbl;
            IdUsuario = id;
            
        }

        public void BuscarDispositivos()
        {
            DispositivoDeVideo = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (DispositivoDeVideo.Count == 0)
            {
                ExisteDispositivo = false;
            }
            else
            {
                ExisteDispositivo = true;
                CargarDispositivos(DispositivoDeVideo);
            }
        }
        public void CargarDispositivos(FilterInfoCollection Dispositivos)
        {
            for (int i = 0; i < Dispositivos.Count; i++)
            {
                comboBox1.Items.Add(Dispositivos[0].Name.ToString());
                comboBox1.Text = comboBox1.Items[0].ToString();
            }
        }

        public void Video_NuevoFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap Imagen = (Bitmap)eventArgs.Frame.Clone();
            picCam.Image = Imagen;
        }

        public void TerminarFuenteDeVideo()
        {
            if (!(FuenteDeVideo == null))
            {
                if (FuenteDeVideo.IsRunning)
                {
                    FuenteDeVideo.SignalToStop();
                    FuenteDeVideo = null;
                }
            }
        }

        

        private async void btnIniciar_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (btnIniciar.Text == "Iniciar")
                {
                    if (FuenteDeVideo != null)
                    {
                        if (FuenteDeVideo.IsRunning)
                        {
                            TerminarFuenteDeVideo();
                        }
                    }


                    if (ExisteDispositivo)
                    {
                        
                        status.BackColor = Color.Orange;
                        lbl_status.Text = "Esperando Captura de Foto...";
                        MessageBox.Show("Abriendo camara, espera a que cargue la imagen.");

                        FuenteDeVideo = new VideoCaptureDevice(DispositivoDeVideo[comboBox1.SelectedIndex].MonikerString);
                        FuenteDeVideo.NewFrame += new NewFrameEventHandler(Video_NuevoFrame);
                        FuenteDeVideo.Start();

                        btnIniciar.Text = "Capturar";
                        comboBox1.Enabled = false;
                    }
                    else
                    {
                        MessageBox.Show("No se puede iniciar, intentelo de nuevo. Probablemente no exista un dispositivo compatible conectado.");
                        status.BackColor = Color.LightSteelBlue;
                        lbl_status.Text = "Sin Acciones";
                        this.Dispose();
                    }




                }
                else
                {
                    btnIniciar.Enabled = false;
                    if (FuenteDeVideo.IsRunning)
                    {
                        TerminarFuenteDeVideo();
                        btnIniciar.Text = "Iniciar";
                        comboBox1.Enabled = true;

                        if (picCam.Image != null)
                        {

                            //convertir a base 64
                            var imageStream = new MemoryStream();
                            picCam.Image.Save(imageStream, ImageFormat.Png);
                            imageStream.Position = 0;
                            var imageBytes = imageStream.ToArray();
                            
                            //subir por el web service
                            string res=await RestClient.SubirFoto(Convert.ToBase64String(imageBytes),IdUsuario.ToString());
                            MessageBox.Show(res);
                            pb.Image = picCam.Image;
                            btnIniciar.Enabled = true;
                            status.BackColor = Color.LightSteelBlue;
                            lbl_status.Text = "Foto guardada en la base de datos";
                            this.Dispose();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excepcion"+ ex.Message);
            }
        }

        private void CapturaFoto_Load(object sender, EventArgs e)
        {

        }

        private void CapturaFoto_FormClosed(object sender, FormClosedEventArgs e)
        {
            TerminarFuenteDeVideo();
            status.BackColor = Color.LightSteelBlue;
            lbl_status.Text = "Sin Acciones";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
