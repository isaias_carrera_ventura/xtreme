﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XtremeGym2._0
{
    public partial class CargandoTempletes : Form
    {
        public CargandoTempletes()
        {
            InitializeComponent();
        }

        private async void CargandoTempletes_Load(object sender, EventArgs e)
        {
            await cargar();
            CapturaHuella ch = new CapturaHuella();
            ch.Show();
            this.Close();
        }

        Task cargar() {
            return Task.Run(()=>{

                Modulo.iniciar();
                Modulo.Conectar();
                if (Modulo.isConnected)
                {
                    if (Modulo.DbInit())//inicia la db del lector
                    {

                        //consulta para traer todas las huellas registradas
                        if (Modulo.CargarTempletes(Modelo.Templetes()))
                        {
                            //MessageBox.Show("Templetes cargados");
                            
                            

                        }
                        else
                        {
                            //error al cargar los templates 
                            MessageBox.Show("Error al cargar las huellas");
                        }
                    }
                    else
                    {
                        //mandar error de inciio de db 
                        MessageBox.Show("Error al iniciar el lector, vuelva a conectarlo e intentelo de nuevo");
                    }
                }
                else
                {
                    //mandar error de conexion con el dispositivo 
                    MessageBox.Show("Error al iniciar el lector, por favor reconectelo e intentelo de nuevo");
                }
            });
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
