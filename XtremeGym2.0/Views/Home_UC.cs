﻿using System;
using System.Windows.Forms;
using zkemkeeper;
using System.Data;
using XtremeGym2._0.FStack;
using System.Collections.Generic;
using System.Threading;
using XtremeGym2._0.Views;
using XtremeGym2._0.App;
using System.Net;

namespace XtremeGym2._0
{
    public partial class Home_UC : UserControl
    {

        private Label lbl_status;
        private int selectedDoor = -1;
        private int selectedReason = -1;

        //Llamada al API
        EventDatabaseRequest eventRequest = new EventDatabaseRequest();

        public Label Lbl_status { get => lbl_status; set => lbl_status = value; }
        private ModuloTorniquete moduloTorniquete;
        private ModuloRegaderas moduloRegaderas;

        internal void enableControlsToOpenMainDoor(ModuloTorniquete moduloTorniquete)
        {

            this.moduloTorniquete = moduloTorniquete;

            comboDoorOpenning.Enabled = true;
            comboReasonOpenning.Enabled = true;
            buttonOpenDoor.Enabled = true;
            buttonUpdateDevice.Enabled = true;

        }


        public void disableControlsToOpenMainDoor()
        {
            comboDoorOpenning.Enabled = false;
            comboReasonOpenning.Enabled = false;
            buttonOpenDoor.Enabled = false;
            buttonUpdateDevice.Enabled = false;
        }

        public Home_UC()
        {

            InitializeComponent();
        }

        private void Home_UC_Load(object sender, EventArgs e)
        {

        }

        private void ButtonOpenDoor_Click(object sender, EventArgs e)
        {

            if (selectedDoor != -1 && selectedReason != -1)
            {

                string description = textBoxDescripcion.Text.Trim();

                //si seleccionamos el "OTRO" como razón debemos escribir algo.
                if(selectedReason == 2 && description.Length == 0)
                {
                    MessageBox.Show("Escribe una razón de apertura.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    controlDoor(selectedDoor, selectedReason, description);
                }

            }
            else
            {
                MessageBox.Show("Selecciona una puerta y un motivo de apertura", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ComboDoorOpenning_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            selectedDoor = combo.SelectedIndex + 1;
        }

        private void ComboReasonOpenning_SelectedIndexChanged(object sender, EventArgs e)
        {

            ComboBox combo = (ComboBox)sender;
            selectedReason = combo.SelectedIndex;

            if (selectedReason == 2)
            {
                textBoxDescripcion.Enabled = true;
            }
            else
            {
                textBoxDescripcion.Enabled = false;
            }
        }


        /**
         * Abre y cierra la puerta.
         */
        private void controlDoor(int door, int reason, string description)
        {
            if (moduloTorniquete != null)
            {

                lbl_status.Text = "Abriendo puerta...";

                Thread thread = new Thread(() =>
                {

                    int statusExec = moduloTorniquete.openDoorManually(door);

                    this.BeginInvoke(
                            (Action<int>)((status) =>
                            {

                                if (status == Constants.SUCCESS)
                                {

                                    eventRequest.sendTorniqueteEvent(reason, door, description);
                                    lbl_status.Text = "Sin acciones...";
                                    MessageBox.Show("Puerta abierta", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    textBoxDescripcion.Text = string.Empty;

                                }
                                else
                                {
                                    lbl_status.Text = "Error al ejecutar apertura de puerta: " + status;
                                }

                                resetFieldsInComboBox();

                            }),
                            new object[] { statusExec }
                          );

                });

                thread.Start();

            }
        }

        /**
         * Reseta los campos en los campos combo box.
         */
        private void resetFieldsInComboBox()
        {

            selectedDoor = -1;
            selectedReason = -1;
            comboDoorOpenning.ResetText();
            comboDoorOpenning.SelectedIndex = -1;
            comboReasonOpenning.ResetText();
            comboReasonOpenning.SelectedIndex = -1;

        }

        private void ButtonRegaderas_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Quieres actualizar los registros de las regaderas? Este proceso podría tardar", "Actualizar Regaderas Mujeres", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {

                using (formWait frm = new formWait(updateAccessInformationForFermale))
                {
                    frm.ShowDialog(this);
                }

            }
        }

        private void updateAccessInformationForMale()
        {
            moduloRegaderas = new ModuloRegaderas();
            if (moduloRegaderas.updateTerminalForMaleGender())
            {
                MessageBox.Show("¡Dispositivo actualizado, los usuarios con paquete de regaderas ya tienen acceso!");
            }
            else
            {
                MessageBox.Show("¡Algo salió mal, verifica que estés conectado a la red de XtremeGym-Hombres!");
            }
        }
        private void updateAccessInformationForFermale()
        {
            moduloRegaderas = new ModuloRegaderas();
            if (moduloRegaderas.updateTerminalForFemaleGender())
            {
                MessageBox.Show("¡Dispositivo actualizado, los usuarios con paquete de regaderas ya tienen acceso!");
            }
            else
            {
                MessageBox.Show("¡Algo salió mal, verifica que estés conectado a la red de XtremeGym-Mujeres!");
            }
        }

        private void ButtonRegaderasHombres_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Quieres actualizar los registros de las regaderas? Este proceso podría tardar", "Actualizar Regaderas Hombres", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                using (formWait frm = new formWait(updateAccessInformationForMale))
                {
                    frm.ShowDialog(this);
                }
            }
        }

        private void ButtonUpdateDevice_Click(object sender, EventArgs e)
        {

            DialogResult result = MessageBox.Show("¿Quieres actualizar los registros del torniquete? Este proceso podría tardar", "Actualizar torniquete", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                updateUserAccessForEntrance();
            }

        }


        private void updateUserAccessForEntrance()
        {

            if (moduloTorniquete != null)
            {

                lbl_status.Text = "Actualizando...";

                Thread thread = new Thread(() =>
                {

                    Modelo modelo = new Modelo();
                    List<string> idUserActiveList = modelo.getIDListActiveUsers();
                    bool flagInfoUpdated = moduloTorniquete.updateUserInformationInBatch(idUserActiveList);

                    this.BeginInvoke(
                            (Action<bool>)((flag) =>
                            {

                                lbl_status.Text = "Sin acciones";

                                if (flag)
                                {

                                    MessageBox.Show("Registros actualizados", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                }
                                else
                                {

                                    MessageBox.Show("Información no guardada.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }

                            }),
                            new object[] { flagInfoUpdated }
                          );

                });

                thread.Start();

            }
        }

    }
}
