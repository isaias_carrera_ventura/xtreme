﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;

namespace XtremeGym2._0
{
    public partial class CapturaHuella : Form
    {
        Detalle_UC detalle_UC;
        fingerprint fp;
        Modelo model;

        public CapturaHuella(Detalle_UC detalle, fingerprint fp2, Modelo m)
        {
            InitializeComponent();
            detalle_UC = detalle;
            fp = fp2;
            model = m;

        }

        public CapturaHuella() {
            InitializeComponent();
            model = new Modelo();

        }

        private void CapturaHuella_Load(object sender, EventArgs e)
        {

        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            btnIniciar.Enabled = false;
            Status.BackColor = Color.Yellow;
            Lbl_status.Text = "Esperando huella dactilar";
            Modulo.IniciarLectura();
            EsperarHuellaYContinuar(new Action(CompararHuella));

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }


        void EsperarHuellaYContinuar(Action nextAction) {
            bool aux = true;
            Thread thread = new Thread(() =>
            {

                while (aux)
                {
                    if (Modulo.bIsTimeToDie)
                    {
                        aux = false;
                    }
                }
                this.BeginInvoke(new Action(nextAction));
            });

            thread.Start();
        }

        void CompararHuella() {

            cambiarMsg();
            int result = Modulo.BuscarPorHuella();
            if (result == -1)
            {
                MessageBox.Show("Parece que el usuario no esta registrado");
                this.Dispose();
            }
            else {
                Status.BackColor = Color.LightSteelBlue;
                Lbl_status.Text = "Cargando información";

                DataTable t = (DataTable)model.getIdUsuarioByTemplate(result);
                int idcliente = int.Parse(t.Rows[0][0].ToString());
                var nombrecliente = t.Rows[0][1].ToString();

                DataTable tc = (DataTable)model.getIdCarro(idcliente);
                int total = int.Parse(tc.Rows[0][0].ToString());

                MessageBox.Show("Cliente: " + idcliente+", "+ nombrecliente);

                if (total == 0)
                {
                    Thread thread = new Thread(() =>
                    {
                        Modelo modelo = new Modelo();
                        modelo.Registro_Carro(idcliente);

                    });
                    thread.Start();

                    MessageBox.Show("Se registro con exito el acceso del usuario", "Registro exitoso");
                }
            }
        }

        async void cambiarMsg (){
            Lbl_status.Text = "Huella capturada... ¡Cargando Datos!";
            Status.BackColor = Color.LightBlue;
        }

        void HuellaCapturada()
        {
        }

        private void btncargar_Click(object sender, EventArgs e)
        {
            CargandoTempletes ct = new CargandoTempletes();
            ct.ShowDialog(this);
            this.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
