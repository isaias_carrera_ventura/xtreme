﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace XtremeGym2._0
{
    public partial class CapturaHuellaEmpleado : Form
    {
        Detalle_UC detalle_UC;
        fingerprint fp;
        Modelo model;

        public CapturaHuellaEmpleado(Detalle_UC detalle, fingerprint fp2, Modelo m)
        {
            InitializeComponent();
            detalle_UC = detalle;
            fp = fp2;
            model = m;

        }

        public CapturaHuellaEmpleado() {
            InitializeComponent();
            model = new Modelo();

        }

        private void CapturaHuella_Load(object sender, EventArgs e)
        {

        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            btnIniciar.Enabled = false;
            Status.BackColor = Color.Yellow;
            Lbl_status.Text = "Esperando huella dactilar";
            Modulo.IniciarLectura();
            EsperarHuellaYContinuar(new Action(CompararHuella));
           
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }


        void EsperarHuellaYContinuar(Action nextAction) {
            bool aux = true;
            Thread thread = new Thread(() =>
            {

                while (aux)
                {
                    if (Modulo.bIsTimeToDie)
                    {
                        aux = false;
                    }
                }
                this.BeginInvoke(new Action(nextAction));
            });

            thread.Start();
        }

        void CompararHuella() {

            cambiarMsg();
            int result = Modulo.BuscarPorHuella();
            if (result == -1)
            {
                MessageBox.Show("Parece que el empleado no esta registrado");
                this.Dispose();
            }
            else {

                Status.BackColor = Color.LightSteelBlue;
                Lbl_status.Text = "Cargando información";

                DataTable t1 = (DataTable)model.getIdEmpleadoHuella(result);
                int idcliente = int.Parse(t1.Rows[0][0].ToString());

                Thread thread = new Thread(() =>
                {
                    Modelo modelo = new Modelo();
                    modelo.setScheduleForEmployee(idcliente);

                });

                thread.Start();

                MessageBox.Show("Registro correcto", "Horario actualizado.");

                this.Dispose();
            }
        }

        async void cambiarMsg (){
            Lbl_status.Text = "Huella capturada... ¡Cargando Datos!";
            Status.BackColor = Color.LightBlue;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
