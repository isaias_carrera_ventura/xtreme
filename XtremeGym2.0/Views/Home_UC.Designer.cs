﻿namespace XtremeGym2._0
{
    partial class Home_UC
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxDescripcion = new System.Windows.Forms.TextBox();
            this.comboReasonOpenning = new System.Windows.Forms.ComboBox();
            this.comboDoorOpenning = new System.Windows.Forms.ComboBox();
            this.buttonOpenDoor = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonRegaderasMujeres = new System.Windows.Forms.Button();
            this.buttonUpdateDevice = new System.Windows.Forms.Button();
            this.buttonRegaderasHombres = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.textBoxDescripcion);
            this.panel2.Controls.Add(this.comboReasonOpenning);
            this.panel2.Controls.Add(this.comboDoorOpenning);
            this.panel2.Controls.Add(this.buttonOpenDoor);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(27, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(309, 263);
            this.panel2.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(103, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 20);
            this.label1.TabIndex = 20;
            this.label1.Text = "Descripción";
            // 
            // textBoxDescripcion
            // 
            this.textBoxDescripcion.Enabled = false;
            this.textBoxDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDescripcion.Location = new System.Drawing.Point(46, 153);
            this.textBoxDescripcion.Name = "textBoxDescripcion";
            this.textBoxDescripcion.Size = new System.Drawing.Size(224, 26);
            this.textBoxDescripcion.TabIndex = 19;
            // 
            // comboReasonOpenning
            // 
            this.comboReasonOpenning.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboReasonOpenning.FormattingEnabled = true;
            this.comboReasonOpenning.Items.AddRange(new object[] {
            "Visita",
            "Servicio",
            "Otro"});
            this.comboReasonOpenning.Location = new System.Drawing.Point(46, 98);
            this.comboReasonOpenning.Name = "comboReasonOpenning";
            this.comboReasonOpenning.Size = new System.Drawing.Size(224, 26);
            this.comboReasonOpenning.TabIndex = 18;
            this.comboReasonOpenning.SelectedIndexChanged += new System.EventHandler(this.ComboReasonOpenning_SelectedIndexChanged);
            // 
            // comboDoorOpenning
            // 
            this.comboDoorOpenning.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboDoorOpenning.FormattingEnabled = true;
            this.comboDoorOpenning.Items.AddRange(new object[] {
            "Salida",
            "Entrada"});
            this.comboDoorOpenning.Location = new System.Drawing.Point(46, 38);
            this.comboDoorOpenning.Name = "comboDoorOpenning";
            this.comboDoorOpenning.Size = new System.Drawing.Size(224, 26);
            this.comboDoorOpenning.TabIndex = 17;
            this.comboDoorOpenning.SelectedIndexChanged += new System.EventHandler(this.ComboDoorOpenning_SelectedIndexChanged);
            // 
            // buttonOpenDoor
            // 
            this.buttonOpenDoor.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonOpenDoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOpenDoor.Location = new System.Drawing.Point(46, 202);
            this.buttonOpenDoor.Name = "buttonOpenDoor";
            this.buttonOpenDoor.Size = new System.Drawing.Size(224, 43);
            this.buttonOpenDoor.TabIndex = 16;
            this.buttonOpenDoor.Text = "Abrir";
            this.buttonOpenDoor.UseVisualStyleBackColor = false;
            this.buttonOpenDoor.Click += new System.EventHandler(this.ButtonOpenDoor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(83, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 20);
            this.label3.TabIndex = 15;
            this.label3.Text = "Motivo de apertura";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(103, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "Puerta Principal";
            // 
            // buttonRegaderasMujeres
            // 
            this.buttonRegaderasMujeres.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonRegaderasMujeres.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegaderasMujeres.Location = new System.Drawing.Point(35, 171);
            this.buttonRegaderasMujeres.Margin = new System.Windows.Forms.Padding(2);
            this.buttonRegaderasMujeres.Name = "buttonRegaderasMujeres";
            this.buttonRegaderasMujeres.Size = new System.Drawing.Size(285, 59);
            this.buttonRegaderasMujeres.TabIndex = 21;
            this.buttonRegaderasMujeres.Text = "Actualizar Regaderas Mujeres";
            this.buttonRegaderasMujeres.UseVisualStyleBackColor = false;
            this.buttonRegaderasMujeres.Click += new System.EventHandler(this.ButtonRegaderas_Click);
            // 
            // buttonUpdateDevice
            // 
            this.buttonUpdateDevice.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonUpdateDevice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateDevice.Location = new System.Drawing.Point(35, 21);
            this.buttonUpdateDevice.Margin = new System.Windows.Forms.Padding(2);
            this.buttonUpdateDevice.Name = "buttonUpdateDevice";
            this.buttonUpdateDevice.Size = new System.Drawing.Size(285, 57);
            this.buttonUpdateDevice.TabIndex = 22;
            this.buttonUpdateDevice.Text = "Actualizar Registros Torniquete";
            this.buttonUpdateDevice.UseVisualStyleBackColor = false;
            this.buttonUpdateDevice.Click += new System.EventHandler(this.ButtonUpdateDevice_Click);
            // 
            // buttonRegaderasHombres
            // 
            this.buttonRegaderasHombres.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonRegaderasHombres.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegaderasHombres.Location = new System.Drawing.Point(35, 98);
            this.buttonRegaderasHombres.Margin = new System.Windows.Forms.Padding(2);
            this.buttonRegaderasHombres.Name = "buttonRegaderasHombres";
            this.buttonRegaderasHombres.Size = new System.Drawing.Size(285, 54);
            this.buttonRegaderasHombres.TabIndex = 23;
            this.buttonRegaderasHombres.Text = "Actualizar Regaderas Hombres";
            this.buttonRegaderasHombres.UseVisualStyleBackColor = false;
            this.buttonRegaderasHombres.Click += new System.EventHandler(this.ButtonRegaderasHombres_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.buttonUpdateDevice);
            this.panel1.Controls.Add(this.buttonRegaderasMujeres);
            this.panel1.Controls.Add(this.buttonRegaderasHombres);
            this.panel1.Location = new System.Drawing.Point(398, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(356, 263);
            this.panel1.TabIndex = 24;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Home_UC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "Home_UC";
            this.Size = new System.Drawing.Size(773, 302);
            this.Load += new System.EventHandler(this.Home_UC_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboReasonOpenning;
        private System.Windows.Forms.ComboBox comboDoorOpenning;
        private System.Windows.Forms.Button buttonOpenDoor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonRegaderasMujeres;
        private System.Windows.Forms.Button buttonUpdateDevice;
        private System.Windows.Forms.Button buttonRegaderasHombres;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDescripcion;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}
