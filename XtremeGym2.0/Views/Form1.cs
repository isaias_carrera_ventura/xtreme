﻿using System;
using System.Windows.Forms;
using System.Threading;
using XtremeGym2._0.FStack;
using XtremeGym2._0.App;

namespace XtremeGym2._0
{

    public partial class Form1 : Form
    {
        Modelo modelo;
        static ModuloTorniquete moduloTorniquete;

        public Form1()
        {

            InitializeComponent();

            this.conectToolStrip.Enabled = false;
            this.disconectToolStrip.Enabled = false;

            try
            {

                modelo = new Modelo();
                moduloTorniquete = new ModuloTorniquete();
                EventDatabaseRequest eventRequest = new EventDatabaseRequest();

                try
                {
                    eventRequest.sendLogToServer();
                }
                catch (Exception)
                {

                }

                disableOpenningControls();

            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message.ToString());
            }



            this.usuarios_UC1.Lbl_status= lbl_Status;
            this.home_UC1.Lbl_status = lbl_Status;
            this.accesoUsuario1.Lbl_status = lbl_Status;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }


        /**
         * Desconecta el dispositivo
         * */
        public void disconnectDevice()
        {

            fingerprint.cerrar();
            moduloTorniquete.disconnectDevice();
            disableOpenningControls();

        }

        /**
         * Connecta con el dispositov.
         */
        private void connectDevice()
        {

            lbl_Status.Text = "Conectando...";

            Thread thread = new Thread(() =>
            {

                int connectionStatus = moduloTorniquete.connectDevice();
                bool flag = connectionStatus == Constants.DEVICE_CONNECTED_SUCCESS;

                this.BeginInvoke(
                        (Action<int>)((status) =>
                        {

                            if (status == Constants.DEVICE_CONNECTED_SUCCESS)
                            {

                                enableOpenningControls();
                                lbl_Status.Text = "Sin acciones...";

                            }
                            else
                            {

                                disableOpenningControls();
                                lbl_Status.Text = "Error de conexión: " + status;
                            }

                        }),
                        new object[] { connectionStatus }
                      );

            });

            thread.Start();

        }


        private void enableOpenningControls()
        {

            MessageBox.Show("Dispositivo conectado correctamente");

            conectToolStrip.Enabled = false;
            disconectToolStrip.Enabled = true;

            home_UC1.enableControlsToOpenMainDoor(moduloTorniquete);
            accesoUsuario1.readLogInTimer(moduloTorniquete);
            usuarios_UC1.setModuloTorniquete(moduloTorniquete);

        }
        private void disableOpenningControls()
        {

            conectToolStrip.Enabled = true;
            disconectToolStrip.Enabled = false;

            home_UC1.disableControlsToOpenMainDoor();
            accesoUsuario1.stopReadLogInTimer();
            usuarios_UC1.setModuloTorniquete(null);

        }

        //Desconectar torniquete
        private void DesconectarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            disconnectDevice();
        }

        //Conectar torniquete
        private void ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            connectDevice();
        }

        //Minimizar ventan
        private void Button3_Click(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Minimized;
        }

        //Cerrar ventan
        private void Button4_Click(object sender, EventArgs e)
        {

            disconnectDevice();
            this.Dispose();

        }

        private void EmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CargandoTempletesEmpleado ct = new CargandoTempletesEmpleado();
            ct.ShowDialog(this);

        }
    }
}
