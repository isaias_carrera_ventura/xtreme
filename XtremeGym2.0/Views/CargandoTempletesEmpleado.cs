﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XtremeGym2._0
{
    public partial class CargandoTempletesEmpleado : Form
    {
        public CargandoTempletesEmpleado()
        {
            InitializeComponent();
        }

        private async void CargandoTempletes_Load(object sender, EventArgs e)
        {
            await cargar();
        }

        Task cargar() {
            return Task.Run(()=>{

                Modulo.iniciar();
                Modulo.Conectar();
                if (Modulo.isConnected)
                {
                    if (Modulo.DbInit())//inicia la db del lector
                    {

                        //consulta para traer todas las huellas registradas
                        if (Modulo.CargarTempletesEmpleado(Modelo.TempletesEmpleado()))
                        {

                            openDialogToReadDactilarFingerprint();
                            
                        }
                        else
                        {
                            //error al cargar los templates 
                            MessageBox.Show("Error al cargar las huellas");
                            closeDialog();
                        }
                    }
                    else
                    {
                        //mandar error de inciio de db 
                        MessageBox.Show("Error al iniciar el lector, vuelva a conectarlo e intentelo de nuevo");
                        closeDialog();
                    }
                }
                else
                {
                    //mandar error de conexion con el dispositivo 
                    MessageBox.Show("Error al iniciar el lector, por favor reconectelo e intentelo de nuevo");

                    closeDialog();

                }
            });
            
        }

        private void openDialogToReadDactilarFingerprint()
        {
            try
            {

                this.BeginInvoke(
                        (Action<int>)((status) =>
                        {

                            CapturaHuellaEmpleado ch = new CapturaHuellaEmpleado();
                            ch.Show();
                            this.Close();

                        }),
                        new object[] { 0 }
                      );

            }
            catch(Exception e)
            {
                MessageBox.Show("Error al iniciar lectura de huella.");
            }

        }

        private void closeDialog()
        {
            
                this.BeginInvoke(
                        (Action<int>)((status) =>
                        {
                            this.Close();

                        }),
                        new object[] { 0 }
                      );
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
