﻿using Sample;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using XtremeGym2._0.Views;
using XtremeGym2._0.App;

namespace XtremeGym2._0
{
    public partial class UsuarioActualiza : Form
    {
        private int ID;
        String templeteString = null;
        DataTable huellas;
        private ModuloTorniquete moduloTorniquete;

        public UsuarioActualiza(int id, ModuloTorniquete modulo)
        {

            InitializeComponent();
            ID = id;
            this.moduloTorniquete = modulo;

        }

        private void UsuarioDetalle_Load(object sender, EventArgs e)
        {

            setViewForUser();
            setFingerPrintsForUser();

            if (Modulo.iniciar() == 0)
            {
                if(Modulo.Conectar() != IntPtr.Zero)
                {
                    buttonConnectDevice.Enabled = false;
                    buttonDisconnectDevice.Enabled = true;
                }
                else
                {
                    buttonConnectDevice.Enabled = true;
                    buttonDisconnectDevice.Enabled = false;
                }
            }
            else
            {

                buttonConnectDevice.Enabled = true;
                buttonDisconnectDevice.Enabled = false;
            }
        }

        private void setViewForUser()
        {

            Modelo modelo = new Modelo();
            DataTable tabla = modelo.getUsuarioById(ID);
            txt_Nombre.Text = tabla.Rows[0][3].ToString();
            txt_ApellidoPaterno.Text = tabla.Rows[0][4].ToString();
            txt_ApellidoMaterno.Text = tabla.Rows[0][5].ToString();
            txt_edad.Text = tabla.Rows[0][6].ToString();
            textBox1.Text = tabla.Rows[0][7].ToString();
            txt_Correo.Text = tabla.Rows[0][8].ToString();
            txt_Telefono.Text = tabla.Rows[0][9].ToString();
            txt_Calle.Text = tabla.Rows[0][10].ToString();
            txt_Numero.Text = tabla.Rows[0][11].ToString();
            txt_Colonia.Text = tabla.Rows[0][12].ToString();
            txt_Ciudad.Text = tabla.Rows[0][14].ToString();
            txt_CP.Text = tabla.Rows[0][13].ToString();

            //TODO: Cargar de otra forma
            //cargarimagen(tabla.Rows[0][1].ToString());
            //cargarimagen2(tabla.Rows[0][2].ToString());


            if (modelo.isUserEmployee(ID))
            {

                groupBox3.Visible = false;
                groupBoxEmployee.Visible = true;
                pictureBoxEmployeeStatus.Image = Properties.Resources.WX_circle_green;
                buttonAccessUpdate.Visible = true;

            }
            else
            {

                DataTable t = modelo.ObtenerPaqueteById(ID);
                int final = t.Rows.Count - 1;

                if (final >= 0)
                {

                    groupBox3.Visible = true;

                    label16.Text = t.Rows[final][0].ToString();
                    label18.Text = t.Rows[final][2].ToString().Substring(0, 10);
                    label22.Text = t.Rows[final][3].ToString().Substring(0, 10);

                    int añoFinal = int.Parse(t.Rows[final][3].ToString().Substring(6, 4));
                    int mesFinal = int.Parse(t.Rows[final][3].ToString().Substring(3, 2));
                    int diaFinal = int.Parse(t.Rows[final][3].ToString().Substring(0, 2));

                    DateTime finalDate = new DateTime(añoFinal, mesFinal, diaFinal);

                    DateTime dateAllowedInitial = new DateTime(añoFinal, mesFinal, diaFinal).AddDays(-5);
                    DateTime dateAllowedFinal = new DateTime(añoFinal, mesFinal, diaFinal).AddDays(5);

                    if (DateTime.Now >= dateAllowedInitial && DateTime.Now <= dateAllowedFinal)
                    {

                        pictureBox1.Image = Properties.Resources.WX_circle_yellow;
                        buttonAccessUpdate.Visible = true;

                    }
                    else if (finalDate >= DateTime.Now)
                    {

                        pictureBox1.Image = Properties.Resources.WX_circle_green;
                        buttonAccessUpdate.Visible = true;

                    }
                    else
                    {

                        pictureBox1.Image = Properties.Resources.WX_circle_red;
                        buttonAccessUpdate.Visible = false;

                    }

                }
                else
                {
                    groupBox3.Visible = false;
                    buttonAccessUpdate.Visible = false;
                }



            }


        }

        private void setFingerPrintsForUser()
        {

            Modelo modelo = new Modelo();
            huellas = modelo.getHuellasByIdAndFinger(ID,ModuloTorniquete.FingerLeft);
            labelFingerLeft.Text = "H: "+huellas.Rows.Count;

            if (huellas.Rows.Count >= 3)
            {
                btnIzq.Enabled = false;
            }

            huellas = modelo.getHuellasByIdAndFinger(ID,ModuloTorniquete.FingerRight);
            labelFingerRight.Text = "H: " + huellas.Rows.Count;

            if (huellas.Rows.Count >= 3)
            {
                btnDer.Enabled = false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            click(ModuloTorniquete.FingerRight);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            click(ModuloTorniquete.FingerLeft);
        }

        void click(int btn)
        {
            if (Modulo.isConnected)
            {
                Lbl_status.Text = "Iniciando lectura ";
                Modulo.IniciarLectura();
                btnDer.Enabled = false;
                btnIzq.Enabled = false;
                Lbl_status.Text = "Esperando huella dactilar...";
                pbLoading.Image = Properties.Resources.loading;
                EsperarCapturaDeHuella(btn);
            }
            else
            {
                Status.BackColor = Color.Red;
                Lbl_status.Text = "ERROR: No se pudo conectar con el dispositivo";
            }
        }

        private static int selectedFinger = 0;

        void EsperarCapturaDeHuella(int btn)
        {

            bool aux = true;
            selectedFinger = btn;

            Thread thread = new Thread(() =>
            {

                while (aux)
                {
                    if (Modulo.bIsTimeToDie)
                    {
                        MemoryStream ms = new MemoryStream();
                        BitmapFormat.GetBitmap(Modulo.FPBuffer, Modulo.mfpWidth, Modulo.mfpHeight, ref ms);
                        Bitmap bmp = new Bitmap(ms);
                        if (btn == 1)
                        {
                            pictureBoxIzq.Image = bmp;
                        }
                        else
                        {
                            pictureBoxDer.Image = bmp;
                        }

                        aux = false;
                    }
                }
                this.BeginInvoke(new Action(HuellaCapturada));
            });

            thread.Start();
        }

        void HuellaCapturada()
        {
            Lbl_status.Text = "Subiendo a la Base de datos...";
            pbLoading.Image = Properties.Resources.cloudres;
            subirTemplate();
        }

        void subirTemplate()
        {
            Thread thread = new Thread(() =>
            {
                Modelo modelo = new Modelo();
                modelo.subirTemplate(Modulo.base64Template, ID, selectedFinger);
                this.BeginInvoke(new Action(CargaTempleteFinalizada));
            });

            thread.Start();
        }

        void CargaTempleteFinalizada()
        {
            pbLoading.Image = Properties.Resources.success;
            Thread thread = new Thread(() =>
            {
                Thread.Sleep(1000);
                this.BeginInvoke(new Action(ResetLbl));
            });
            thread.Start();
        }

        void ResetLbl()
        {
            btnIzq.Enabled = true;
            btnDer.Enabled = true;
            Status.BackColor = Color.LightSteelBlue;
            Lbl_status.Text = "Sin acciones";
            pbLoading.Image = null;
            Modulo.Reset();
            pictureBoxIzq.Image = null;
            pictureBoxDer.Image = null;
            setFingerPrintsForUser();
            selectedFinger = 0;

        }


        //Metodos para cargar imagen
        private Stream getUrl(string URL)
        {

            HttpWebRequest request = ((HttpWebRequest)WebRequest.Create(URL));
            HttpWebResponse response = ((HttpWebResponse)request.GetResponse());

            try
            {

                return response.GetResponseStream();

            }
            catch
            {
                return response.GetResponseStream();
            }

        }
        private void cargarimagen(string nombre)
        {
            Stream StreamImagen;
            string elError = "";
            StreamImagen = getUrl("https://www.xtremegym.com.mx/gimnasio/assets/imagenes/clientes/" + nombre);
            if (elError == "")
            {
                pictureBox2.Image = System.Drawing.Image.FromStream(StreamImagen);
            }
        }

        private void cargarimagen2(string nombre)
        {
            Stream StreamImagen;
            string elError = "";
            StreamImagen = getUrl("https://www.xtremegym.com.mx/gimnasio/assets/imagenes/clientes/" + nombre);
            if (elError == "")
            {
                pictureBox3.Image = System.Drawing.Image.FromStream(StreamImagen);
            }
        }
        private void UsuarioDetalle_FormClosing(object sender, FormClosingEventArgs e)
        {

            Modulo.Desconectar();

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Deseas actualizar la foto del usuario?", "Actualizar foto",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Lbl_status.Text = "Esperando Captura de Foto";
                CapturaFoto cf = new CapturaFoto(pictureBox2, Status, Lbl_status, ID);
                cf.ShowDialog(this);
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Deseas actualizar la foto de la identificación?", "Actualizar identificación",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Lbl_status.Text = "Esperando Captura de Identificación";
                CapturaIdentificacion cf = new CapturaIdentificacion(pictureBox3, Status, Lbl_status, ID);
                cf.ShowDialog(this);
            }

        }

        private void ButtonAccessUpdate_Click(object sender, EventArgs e)
        {
            using (formWait frm = new formWait(updateAccessInformation))
            {
                frm.ShowDialog(this);
            }

        }

        private void updateAccessInformation()
        {

            if (moduloTorniquete != null)
            {

                Thread.Sleep(1000);

                Thread thread = new Thread(() =>
                {

                    string descriptionExec = "";
                    Dictionary<string, string> dictionaryDates = Modelo.getInitialAndFinalDatesForUserId(ID);
                    Modelo modelo = new Modelo();
                    huellas = modelo.getHuellasById(ID);

                    bool flagInfo = false;

                    if (dictionaryDates != null)
                    {
                        flagInfo = moduloTorniquete.updateUserInfoInDevice(ID, dictionaryDates["initialDate"], dictionaryDates["finalDate"]);
                    }

                    for(int i = 0; i < huellas.Rows.Count; i++)
                    {
                        //ID Usuario, Finger ID , FingerPrint
                        moduloTorniquete.updateUserTemplateInDevice(ID, int.Parse(huellas.Rows[i][4].ToString()), huellas.Rows[i][2].ToString());
                    }

                    bool flagAccess = moduloTorniquete.updateUserAuthorizeInDoor(ID);

                    if (flagInfo && flagAccess)
                    {
                        descriptionExec += "Datos actualizados, el usuario ya tiene acceso hasta el día:" + dictionaryDates["finalDate"];
                    }
                    else
                    {
                        descriptionExec += "Algo salió mal, contacta a soporte.";
                    }

                    this.BeginInvoke(
                                (Action<string>)((description) =>
                                {

                                    MessageBox.Show(descriptionExec);

                                }),
                                new object[] { descriptionExec }
                              );


                });

                thread.Start();
            }
            else
            {

                MessageBox.Show("No pudimos conectarnos al torniquete");

            }


        }

        private void ButtonUpdateEmployee_Click(object sender, EventArgs e)
        {

            using (formWait frm = new formWait(updateAccessInformationForEmployee))
            {
                frm.ShowDialog(this);
            }

        }

        private void updateAccessInformationForEmployee()
        {
            if (moduloTorniquete != null)
            {

                Thread.Sleep(1000);
                Thread thread = new Thread(() =>
                {

                    string descriptionExec = "";
                    Modelo modelo = new Modelo();
                    huellas = modelo.getHuellasById(ID);

                    bool flagInfo = moduloTorniquete.updateUserInfoInDevice(ID, null, null);

                    for (int i = 0; i < huellas.Rows.Count; i++)
                    {
                        //ID Usuario, Finger ID , FingerPrint
                        moduloTorniquete.updateUserTemplateInDevice(ID, int.Parse(huellas.Rows[i][4].ToString()), huellas.Rows[i][2].ToString());
                    }

                    bool flagAccess = moduloTorniquete.updateUserAuthorizeInDoor(ID);

                    if (flagInfo && flagAccess)
                    {
                        descriptionExec += "Datos actualizados, el usuario ya tiene acceso";
                    }
                    else
                    {
                        descriptionExec += "Algo salió mal, contacta a soporte.";
                    }

                    this.BeginInvoke(
                                (Action<string>)((description) =>
                                {

                                    MessageBox.Show(descriptionExec);

                                }),
                                new object[] { descriptionExec }
                              );

                });

                thread.Start();
            }
            else
            {
                MessageBox.Show("No pudimos conectarnos al torniquete");
            }
        }

        private void ButtonDiscardFingerprints_Click(object sender, EventArgs e)
        {

            DialogResult result = MessageBox.Show("¿Quieres eliminar las huellas de este usuario?", "Eliminar Huellas", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.Equals(DialogResult.OK))
            {
                discardFingerprintsForUser();
            }

        }

        private void discardFingerprintsForUser()
        {

            Modelo modelo = new Modelo();

            if (modelo.discardFingerprintsForUser(ID))
            {

                pictureBoxIzq.Image = null;
                pictureBoxDer.Image = null;

                btnDer.Enabled = true;
                btnIzq.Enabled = true;

                setFingerPrintsForUser();

            }
            else
            {

                MessageBox.Show("No pudimos eliminar las huellas");
            }


        }

        private void ButtonConnectDevice_Click(object sender, EventArgs e)
        {
            if (Modulo.iniciar() == 0)
            {
                if (Modulo.Conectar() != IntPtr.Zero)
                {
                    buttonConnectDevice.Enabled = false;
                    buttonDisconnectDevice.Enabled = true;
                }
                else
                {
                    buttonConnectDevice.Enabled = true;
                    buttonDisconnectDevice.Enabled = false;
                }
            }
            else
            {

                buttonConnectDevice.Enabled = true;
                buttonDisconnectDevice.Enabled = false;
            }

        }

        private void ButtonDisconnectDevice_Click(object sender, EventArgs e)
        {
            Modulo.Desconectar();
            buttonConnectDevice.Enabled = true;
            buttonDisconnectDevice.Enabled = false;
        }

    }
}
