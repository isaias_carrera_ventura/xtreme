﻿namespace XtremeGym2._0
{
    partial class UsuarioActualiza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label17;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label lbl_registro;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label15;
            System.Windows.Forms.Label label19;
            System.Windows.Forms.Label label20;
            System.Windows.Forms.Label label21;
            System.Windows.Forms.Label label23;
            System.Windows.Forms.Label label24;
            System.Windows.Forms.Label label28;
            this.panel_Direccion = new System.Windows.Forms.GroupBox();
            this.txt_Telefono = new System.Windows.Forms.TextBox();
            this.txt_CP = new System.Windows.Forms.TextBox();
            this.txt_Ciudad = new System.Windows.Forms.TextBox();
            this.txt_Colonia = new System.Windows.Forms.TextBox();
            this.txt_Calle = new System.Windows.Forms.TextBox();
            this.txt_Numero = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txt_Correo = new System.Windows.Forms.TextBox();
            this.txt_edad = new System.Windows.Forms.TextBox();
            this.txt_ApellidoMaterno = new System.Windows.Forms.TextBox();
            this.txt_Nombre = new System.Windows.Forms.TextBox();
            this.txt_ApellidoPaterno = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelFingerRight = new System.Windows.Forms.Label();
            this.labelFingerLeft = new System.Windows.Forms.Label();
            this.btnDer = new System.Windows.Forms.Button();
            this.pictureBoxDer = new System.Windows.Forms.PictureBox();
            this.btnIzq = new System.Windows.Forms.Button();
            this.pictureBoxIzq = new System.Windows.Forms.PictureBox();
            this.buttonDiscardFingerprints = new System.Windows.Forms.Button();
            this.Status = new System.Windows.Forms.Panel();
            this.pbLoading = new System.Windows.Forms.PictureBox();
            this.Lbl_status = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonAccessUpdate = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBoxEmployee = new System.Windows.Forms.GroupBox();
            this.buttonUpdateEmployee = new System.Windows.Forms.Button();
            this.pictureBoxEmployeeStatus = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.buttonConnectDevice = new System.Windows.Forms.Button();
            this.buttonDisconnectDevice = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            label11 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            lbl_registro = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label15 = new System.Windows.Forms.Label();
            label19 = new System.Windows.Forms.Label();
            label20 = new System.Windows.Forms.Label();
            label21 = new System.Windows.Forms.Label();
            label23 = new System.Windows.Forms.Label();
            label24 = new System.Windows.Forms.Label();
            label28 = new System.Windows.Forms.Label();
            this.panel_Direccion.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIzq)).BeginInit();
            this.Status.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBoxEmployee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmployeeStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.BackColor = System.Drawing.Color.Transparent;
            label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label11.Location = new System.Drawing.Point(571, 38);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(64, 17);
            label11.TabIndex = 14;
            label11.Text = "Telefono";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.BackColor = System.Drawing.Color.Transparent;
            label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label10.Location = new System.Drawing.Point(511, 38);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(34, 17);
            label10.TabIndex = 10;
            label10.Text = "C.P.";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.BackColor = System.Drawing.Color.Transparent;
            label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label6.Location = new System.Drawing.Point(381, 38);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(52, 17);
            label6.TabIndex = 8;
            label6.Text = "Ciudad";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.BackColor = System.Drawing.Color.Transparent;
            label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label7.Location = new System.Drawing.Point(238, 38);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(55, 17);
            label7.TabIndex = 6;
            label7.Text = "Colonia";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.BackColor = System.Drawing.Color.Transparent;
            label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label8.Location = new System.Drawing.Point(10, 38);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(39, 17);
            label8.TabIndex = 1;
            label8.Text = "Calle";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.BackColor = System.Drawing.Color.Transparent;
            label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label9.Location = new System.Drawing.Point(169, 38);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(58, 17);
            label9.TabIndex = 4;
            label9.Text = "Numero";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.BackColor = System.Drawing.Color.Transparent;
            label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label17.Location = new System.Drawing.Point(10, 82);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(51, 17);
            label17.TabIndex = 15;
            label17.Text = "Correo";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.BackColor = System.Drawing.Color.Transparent;
            label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label12.Location = new System.Drawing.Point(596, 39);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(39, 17);
            label12.TabIndex = 10;
            label12.Text = "Sexo";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.BackColor = System.Drawing.Color.Transparent;
            label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label5.Location = new System.Drawing.Point(518, 39);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(41, 17);
            label5.TabIndex = 8;
            label5.Text = "Edad";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.BackColor = System.Drawing.Color.Transparent;
            label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label4.Location = new System.Drawing.Point(347, 28);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(114, 17);
            label4.TabIndex = 6;
            label4.Text = "Apellido Materno";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = System.Drawing.Color.Transparent;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label2.Location = new System.Drawing.Point(10, 28);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(75, 17);
            label2.TabIndex = 1;
            label2.Text = "Nombre(s)";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = System.Drawing.Color.Transparent;
            label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label3.Location = new System.Drawing.Point(194, 28);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(112, 17);
            label3.TabIndex = 4;
            label3.Text = "Apellido Paterno";
            // 
            // lbl_registro
            // 
            lbl_registro.AutoSize = true;
            lbl_registro.BackColor = System.Drawing.Color.Transparent;
            lbl_registro.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            lbl_registro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            lbl_registro.Location = new System.Drawing.Point(0, 9);
            lbl_registro.Name = "lbl_registro";
            lbl_registro.Size = new System.Drawing.Size(317, 39);
            lbl_registro.TabIndex = 11;
            lbl_registro.Text = "Detalles del usuario";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.BackColor = System.Drawing.Color.Transparent;
            label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label13.Location = new System.Drawing.Point(199, 34);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(62, 17);
            label13.TabIndex = 17;
            label13.Text = "Derecha";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = System.Drawing.Color.Transparent;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label1.Location = new System.Drawing.Point(47, 30);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(66, 17);
            label1.TabIndex = 16;
            label1.Text = "Izquierda";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.BackColor = System.Drawing.Color.Transparent;
            label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label15.Location = new System.Drawing.Point(596, 38);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(52, 17);
            label15.TabIndex = 14;
            label15.Text = "Estado";
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.BackColor = System.Drawing.Color.Transparent;
            label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label19.Location = new System.Drawing.Point(438, 38);
            label19.Name = "label19";
            label19.Size = new System.Drawing.Size(81, 17);
            label19.TabIndex = 6;
            label19.Text = "Fecha Final";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.BackColor = System.Drawing.Color.Transparent;
            label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label20.Location = new System.Drawing.Point(10, 38);
            label20.Name = "label20";
            label20.Size = new System.Drawing.Size(61, 17);
            label20.TabIndex = 1;
            label20.Text = "Paquete";
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.BackColor = System.Drawing.Color.Transparent;
            label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label21.Location = new System.Drawing.Point(251, 38);
            label21.Name = "label21";
            label21.Size = new System.Drawing.Size(86, 17);
            label21.TabIndex = 4;
            label21.Text = "Fecha inicial";
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.BackColor = System.Drawing.Color.Transparent;
            label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label23.Location = new System.Drawing.Point(23, 61);
            label23.Name = "label23";
            label23.Size = new System.Drawing.Size(99, 25);
            label23.TabIndex = 19;
            label23.Text = "Fotografía";
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.BackColor = System.Drawing.Color.Transparent;
            label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label24.Location = new System.Drawing.Point(188, 60);
            label24.Name = "label24";
            label24.Size = new System.Drawing.Size(125, 25);
            label24.TabIndex = 20;
            label24.Text = "Identificación";
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.BackColor = System.Drawing.Color.Transparent;
            label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            label28.Location = new System.Drawing.Point(596, 38);
            label28.Name = "label28";
            label28.Size = new System.Drawing.Size(52, 17);
            label28.TabIndex = 14;
            label28.Text = "Estado";
            // 
            // panel_Direccion
            // 
            this.panel_Direccion.BackColor = System.Drawing.Color.Transparent;
            this.panel_Direccion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel_Direccion.CausesValidation = false;
            this.panel_Direccion.Controls.Add(this.txt_Telefono);
            this.panel_Direccion.Controls.Add(label11);
            this.panel_Direccion.Controls.Add(this.txt_CP);
            this.panel_Direccion.Controls.Add(label10);
            this.panel_Direccion.Controls.Add(this.txt_Ciudad);
            this.panel_Direccion.Controls.Add(label6);
            this.panel_Direccion.Controls.Add(label7);
            this.panel_Direccion.Controls.Add(this.txt_Colonia);
            this.panel_Direccion.Controls.Add(label8);
            this.panel_Direccion.Controls.Add(this.txt_Calle);
            this.panel_Direccion.Controls.Add(this.txt_Numero);
            this.panel_Direccion.Controls.Add(label9);
            this.panel_Direccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.panel_Direccion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.panel_Direccion.Location = new System.Drawing.Point(342, 164);
            this.panel_Direccion.Name = "panel_Direccion";
            this.panel_Direccion.Size = new System.Drawing.Size(771, 115);
            this.panel_Direccion.TabIndex = 13;
            this.panel_Direccion.TabStop = false;
            this.panel_Direccion.Text = "Dirección";
            // 
            // txt_Telefono
            // 
            this.txt_Telefono.BackColor = System.Drawing.Color.LightGray;
            this.txt_Telefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_Telefono.ForeColor = System.Drawing.Color.Black;
            this.txt_Telefono.Location = new System.Drawing.Point(575, 60);
            this.txt_Telefono.Name = "txt_Telefono";
            this.txt_Telefono.ReadOnly = true;
            this.txt_Telefono.Size = new System.Drawing.Size(120, 23);
            this.txt_Telefono.TabIndex = 15;
            // 
            // txt_CP
            // 
            this.txt_CP.BackColor = System.Drawing.Color.LightGray;
            this.txt_CP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_CP.ForeColor = System.Drawing.Color.Black;
            this.txt_CP.Location = new System.Drawing.Point(515, 60);
            this.txt_CP.Name = "txt_CP";
            this.txt_CP.ReadOnly = true;
            this.txt_CP.Size = new System.Drawing.Size(54, 23);
            this.txt_CP.TabIndex = 11;
            // 
            // txt_Ciudad
            // 
            this.txt_Ciudad.BackColor = System.Drawing.Color.LightGray;
            this.txt_Ciudad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_Ciudad.ForeColor = System.Drawing.Color.Black;
            this.txt_Ciudad.Location = new System.Drawing.Point(385, 60);
            this.txt_Ciudad.Name = "txt_Ciudad";
            this.txt_Ciudad.ReadOnly = true;
            this.txt_Ciudad.Size = new System.Drawing.Size(124, 23);
            this.txt_Ciudad.TabIndex = 9;
            // 
            // txt_Colonia
            // 
            this.txt_Colonia.BackColor = System.Drawing.Color.LightGray;
            this.txt_Colonia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_Colonia.ForeColor = System.Drawing.Color.Black;
            this.txt_Colonia.Location = new System.Drawing.Point(238, 60);
            this.txt_Colonia.Name = "txt_Colonia";
            this.txt_Colonia.ReadOnly = true;
            this.txt_Colonia.Size = new System.Drawing.Size(141, 23);
            this.txt_Colonia.TabIndex = 7;
            // 
            // txt_Calle
            // 
            this.txt_Calle.BackColor = System.Drawing.Color.LightGray;
            this.txt_Calle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_Calle.ForeColor = System.Drawing.Color.Black;
            this.txt_Calle.Location = new System.Drawing.Point(14, 60);
            this.txt_Calle.Name = "txt_Calle";
            this.txt_Calle.ReadOnly = true;
            this.txt_Calle.Size = new System.Drawing.Size(153, 23);
            this.txt_Calle.TabIndex = 3;
            // 
            // txt_Numero
            // 
            this.txt_Numero.BackColor = System.Drawing.Color.LightGray;
            this.txt_Numero.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_Numero.ForeColor = System.Drawing.Color.Black;
            this.txt_Numero.Location = new System.Drawing.Point(173, 60);
            this.txt_Numero.Name = "txt_Numero";
            this.txt_Numero.ReadOnly = true;
            this.txt_Numero.Size = new System.Drawing.Size(59, 23);
            this.txt_Numero.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox1.CausesValidation = false;
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(label17);
            this.groupBox1.Controls.Add(this.txt_Correo);
            this.groupBox1.Controls.Add(label12);
            this.groupBox1.Controls.Add(this.txt_edad);
            this.groupBox1.Controls.Add(label5);
            this.groupBox1.Controls.Add(label4);
            this.groupBox1.Controls.Add(this.txt_ApellidoMaterno);
            this.groupBox1.Controls.Add(label2);
            this.groupBox1.Controls.Add(this.txt_Nombre);
            this.groupBox1.Controls.Add(this.txt_ApellidoPaterno);
            this.groupBox1.Controls.Add(label3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.groupBox1.Location = new System.Drawing.Point(342, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(771, 117);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Personales";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.LightGray;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Location = new System.Drawing.Point(594, 61);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(43, 23);
            this.textBox1.TabIndex = 17;
            // 
            // txt_Correo
            // 
            this.txt_Correo.BackColor = System.Drawing.Color.LightGray;
            this.txt_Correo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_Correo.ForeColor = System.Drawing.Color.Black;
            this.txt_Correo.Location = new System.Drawing.Point(71, 81);
            this.txt_Correo.Name = "txt_Correo";
            this.txt_Correo.ReadOnly = true;
            this.txt_Correo.Size = new System.Drawing.Size(404, 23);
            this.txt_Correo.TabIndex = 16;
            // 
            // txt_edad
            // 
            this.txt_edad.BackColor = System.Drawing.Color.LightGray;
            this.txt_edad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_edad.ForeColor = System.Drawing.Color.Black;
            this.txt_edad.Location = new System.Drawing.Point(522, 61);
            this.txt_edad.Name = "txt_edad";
            this.txt_edad.ReadOnly = true;
            this.txt_edad.Size = new System.Drawing.Size(43, 23);
            this.txt_edad.TabIndex = 9;
            // 
            // txt_ApellidoMaterno
            // 
            this.txt_ApellidoMaterno.BackColor = System.Drawing.Color.LightGray;
            this.txt_ApellidoMaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_ApellidoMaterno.ForeColor = System.Drawing.Color.Black;
            this.txt_ApellidoMaterno.Location = new System.Drawing.Point(343, 50);
            this.txt_ApellidoMaterno.Name = "txt_ApellidoMaterno";
            this.txt_ApellidoMaterno.ReadOnly = true;
            this.txt_ApellidoMaterno.Size = new System.Drawing.Size(132, 23);
            this.txt_ApellidoMaterno.TabIndex = 7;
            // 
            // txt_Nombre
            // 
            this.txt_Nombre.BackColor = System.Drawing.Color.LightGray;
            this.txt_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_Nombre.ForeColor = System.Drawing.Color.Black;
            this.txt_Nombre.Location = new System.Drawing.Point(14, 50);
            this.txt_Nombre.Name = "txt_Nombre";
            this.txt_Nombre.ReadOnly = true;
            this.txt_Nombre.Size = new System.Drawing.Size(170, 23);
            this.txt_Nombre.TabIndex = 3;
            // 
            // txt_ApellidoPaterno
            // 
            this.txt_ApellidoPaterno.BackColor = System.Drawing.Color.LightGray;
            this.txt_ApellidoPaterno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.txt_ApellidoPaterno.ForeColor = System.Drawing.Color.Black;
            this.txt_ApellidoPaterno.Location = new System.Drawing.Point(190, 50);
            this.txt_ApellidoPaterno.Name = "txt_ApellidoPaterno";
            this.txt_ApellidoPaterno.ReadOnly = true;
            this.txt_ApellidoPaterno.Size = new System.Drawing.Size(147, 23);
            this.txt_ApellidoPaterno.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox2.CausesValidation = false;
            this.groupBox2.Controls.Add(this.labelFingerRight);
            this.groupBox2.Controls.Add(this.labelFingerLeft);
            this.groupBox2.Controls.Add(label13);
            this.groupBox2.Controls.Add(label1);
            this.groupBox2.Controls.Add(this.btnDer);
            this.groupBox2.Controls.Add(this.pictureBoxDer);
            this.groupBox2.Controls.Add(this.btnIzq);
            this.groupBox2.Controls.Add(this.pictureBoxIzq);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.groupBox2.Location = new System.Drawing.Point(28, 284);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(308, 281);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Huella Digital";
            // 
            // labelFingerRight
            // 
            this.labelFingerRight.AutoSize = true;
            this.labelFingerRight.Location = new System.Drawing.Point(213, 188);
            this.labelFingerRight.Name = "labelFingerRight";
            this.labelFingerRight.Size = new System.Drawing.Size(48, 25);
            this.labelFingerRight.TabIndex = 24;
            this.labelFingerRight.Text = "H: 0";
            // 
            // labelFingerLeft
            // 
            this.labelFingerLeft.AutoSize = true;
            this.labelFingerLeft.Location = new System.Drawing.Point(65, 184);
            this.labelFingerLeft.Name = "labelFingerLeft";
            this.labelFingerLeft.Size = new System.Drawing.Size(48, 25);
            this.labelFingerLeft.TabIndex = 23;
            this.labelFingerLeft.Text = "H: 0";
            // 
            // btnDer
            // 
            this.btnDer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnDer.Location = new System.Drawing.Point(174, 228);
            this.btnDer.Name = "btnDer";
            this.btnDer.Size = new System.Drawing.Size(110, 36);
            this.btnDer.TabIndex = 7;
            this.btnDer.Text = "Scanear Der.";
            this.btnDer.UseVisualStyleBackColor = true;
            this.btnDer.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBoxDer
            // 
            this.pictureBoxDer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxDer.Location = new System.Drawing.Point(175, 54);
            this.pictureBoxDer.Name = "pictureBoxDer";
            this.pictureBoxDer.Size = new System.Drawing.Size(110, 131);
            this.pictureBoxDer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDer.TabIndex = 6;
            this.pictureBoxDer.TabStop = false;
            // 
            // btnIzq
            // 
            this.btnIzq.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnIzq.Location = new System.Drawing.Point(30, 224);
            this.btnIzq.Name = "btnIzq";
            this.btnIzq.Size = new System.Drawing.Size(110, 36);
            this.btnIzq.TabIndex = 5;
            this.btnIzq.Text = "Scanear Izq.";
            this.btnIzq.UseVisualStyleBackColor = true;
            this.btnIzq.Click += new System.EventHandler(this.button3_Click);
            // 
            // pictureBoxIzq
            // 
            this.pictureBoxIzq.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxIzq.Location = new System.Drawing.Point(31, 54);
            this.pictureBoxIzq.Name = "pictureBoxIzq";
            this.pictureBoxIzq.Size = new System.Drawing.Size(109, 131);
            this.pictureBoxIzq.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxIzq.TabIndex = 4;
            this.pictureBoxIzq.TabStop = false;
            // 
            // buttonDiscardFingerprints
            // 
            this.buttonDiscardFingerprints.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDiscardFingerprints.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonDiscardFingerprints.Location = new System.Drawing.Point(119, 571);
            this.buttonDiscardFingerprints.Name = "buttonDiscardFingerprints";
            this.buttonDiscardFingerprints.Size = new System.Drawing.Size(144, 40);
            this.buttonDiscardFingerprints.TabIndex = 22;
            this.buttonDiscardFingerprints.Text = "Descartar Huellas";
            this.buttonDiscardFingerprints.UseVisualStyleBackColor = true;
            this.buttonDiscardFingerprints.Click += new System.EventHandler(this.ButtonDiscardFingerprints_Click);
            // 
            // Status
            // 
            this.Status.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Status.Controls.Add(this.pbLoading);
            this.Status.Controls.Add(this.Lbl_status);
            this.Status.Controls.Add(this.label14);
            this.Status.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Status.Location = new System.Drawing.Point(0, 634);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(1143, 36);
            this.Status.TabIndex = 18;
            // 
            // pbLoading
            // 
            this.pbLoading.BackColor = System.Drawing.Color.Transparent;
            this.pbLoading.Location = new System.Drawing.Point(1101, 3);
            this.pbLoading.Name = "pbLoading";
            this.pbLoading.Size = new System.Drawing.Size(30, 30);
            this.pbLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLoading.TabIndex = 2;
            this.pbLoading.TabStop = false;
            // 
            // Lbl_status
            // 
            this.Lbl_status.AutoSize = true;
            this.Lbl_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Lbl_status.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Lbl_status.Location = new System.Drawing.Point(116, 8);
            this.Lbl_status.Name = "Lbl_status";
            this.Lbl_status.Size = new System.Drawing.Size(111, 18);
            this.Lbl_status.TabIndex = 1;
            this.Lbl_status.Text = "Sin Acciones.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.75F);
            this.label14.Location = new System.Drawing.Point(3, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 18);
            this.label14.TabIndex = 0;
            this.label14.Text = "Status Actual:";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox3.CausesValidation = false;
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Controls.Add(this.buttonAccessUpdate);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(label15);
            this.groupBox3.Controls.Add(label19);
            this.groupBox3.Controls.Add(label20);
            this.groupBox3.Controls.Add(label21);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.groupBox3.Location = new System.Drawing.Point(342, 295);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(746, 151);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Suscripción";
            this.groupBox3.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(617, 60);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(20, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // buttonAccessUpdate
            // 
            this.buttonAccessUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAccessUpdate.Location = new System.Drawing.Point(514, 104);
            this.buttonAccessUpdate.Name = "buttonAccessUpdate";
            this.buttonAccessUpdate.Size = new System.Drawing.Size(173, 33);
            this.buttonAccessUpdate.TabIndex = 19;
            this.buttonAccessUpdate.Text = "Actualizar Acceso";
            this.buttonAccessUpdate.UseVisualStyleBackColor = true;
            this.buttonAccessUpdate.Click += new System.EventHandler(this.ButtonAccessUpdate_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label22.Location = new System.Drawing.Point(438, 57);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 20);
            this.label22.TabIndex = 17;
            this.label22.Text = "Paquete 1";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label18.Location = new System.Drawing.Point(251, 57);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 20);
            this.label18.TabIndex = 16;
            this.label18.Text = "Paquete 1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label16.Location = new System.Drawing.Point(10, 59);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 20);
            this.label16.TabIndex = 15;
            this.label16.Text = "Paquete 1";
            // 
            // groupBoxEmployee
            // 
            this.groupBoxEmployee.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxEmployee.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBoxEmployee.CausesValidation = false;
            this.groupBoxEmployee.Controls.Add(this.buttonUpdateEmployee);
            this.groupBoxEmployee.Controls.Add(this.pictureBoxEmployeeStatus);
            this.groupBoxEmployee.Controls.Add(label28);
            this.groupBoxEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.groupBoxEmployee.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(106)))), ((int)(((byte)(181)))));
            this.groupBoxEmployee.Location = new System.Drawing.Point(348, 285);
            this.groupBoxEmployee.Name = "groupBoxEmployee";
            this.groupBoxEmployee.Size = new System.Drawing.Size(743, 149);
            this.groupBoxEmployee.TabIndex = 19;
            this.groupBoxEmployee.TabStop = false;
            this.groupBoxEmployee.Text = "Empleado";
            this.groupBoxEmployee.Visible = false;
            // 
            // buttonUpdateEmployee
            // 
            this.buttonUpdateEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateEmployee.Location = new System.Drawing.Point(241, 76);
            this.buttonUpdateEmployee.Name = "buttonUpdateEmployee";
            this.buttonUpdateEmployee.Size = new System.Drawing.Size(234, 47);
            this.buttonUpdateEmployee.TabIndex = 20;
            this.buttonUpdateEmployee.Text = "Actualizar Acceso Empleado";
            this.buttonUpdateEmployee.UseVisualStyleBackColor = true;
            this.buttonUpdateEmployee.Click += new System.EventHandler(this.ButtonUpdateEmployee_Click);
            // 
            // pictureBoxEmployeeStatus
            // 
            this.pictureBoxEmployeeStatus.Location = new System.Drawing.Point(617, 60);
            this.pictureBoxEmployeeStatus.Name = "pictureBoxEmployeeStatus";
            this.pictureBoxEmployeeStatus.Size = new System.Drawing.Size(20, 20);
            this.pictureBoxEmployeeStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxEmployeeStatus.TabIndex = 18;
            this.pictureBoxEmployeeStatus.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(28, 91);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(136, 170);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox3.Location = new System.Drawing.Point(187, 91);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(140, 170);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 21;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // buttonConnectDevice
            // 
            this.buttonConnectDevice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConnectDevice.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonConnectDevice.Location = new System.Drawing.Point(235, 64);
            this.buttonConnectDevice.Name = "buttonConnectDevice";
            this.buttonConnectDevice.Size = new System.Drawing.Size(107, 33);
            this.buttonConnectDevice.TabIndex = 23;
            this.buttonConnectDevice.Text = "Conectar";
            this.buttonConnectDevice.UseVisualStyleBackColor = true;
            this.buttonConnectDevice.Click += new System.EventHandler(this.ButtonConnectDevice_Click);
            // 
            // buttonDisconnectDevice
            // 
            this.buttonDisconnectDevice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDisconnectDevice.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonDisconnectDevice.Location = new System.Drawing.Point(396, 64);
            this.buttonDisconnectDevice.Name = "buttonDisconnectDevice";
            this.buttonDisconnectDevice.Size = new System.Drawing.Size(110, 33);
            this.buttonDisconnectDevice.TabIndex = 24;
            this.buttonDisconnectDevice.Text = "Desconectar";
            this.buttonDisconnectDevice.UseVisualStyleBackColor = true;
            this.buttonDisconnectDevice.Click += new System.EventHandler(this.ButtonDisconnectDevice_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonDisconnectDevice);
            this.groupBox4.Controls.Add(this.buttonConnectDevice);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.groupBox4.Location = new System.Drawing.Point(345, 468);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(768, 122);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Sensor de huellas";
            // 
            // UsuarioActualiza
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 670);
            this.Controls.Add(this.groupBoxEmployee);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.buttonDiscardFingerprints);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(label24);
            this.Controls.Add(label23);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panel_Direccion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(lbl_registro);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "UsuarioActualiza";
            this.Text = "Detalles del usuario";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UsuarioDetalle_FormClosing);
            this.Load += new System.EventHandler(this.UsuarioDetalle_Load);
            this.panel_Direccion.ResumeLayout(false);
            this.panel_Direccion.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIzq)).EndInit();
            this.Status.ResumeLayout(false);
            this.Status.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLoading)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBoxEmployee.ResumeLayout(false);
            this.groupBoxEmployee.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEmployeeStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox panel_Direccion;
        public System.Windows.Forms.TextBox txt_Telefono;
        private System.Windows.Forms.TextBox txt_CP;
        public System.Windows.Forms.TextBox txt_Ciudad;
        public System.Windows.Forms.TextBox txt_Colonia;
        public System.Windows.Forms.TextBox txt_Calle;
        public System.Windows.Forms.TextBox txt_Numero;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox txt_Correo;
        public System.Windows.Forms.TextBox txt_edad;
        private System.Windows.Forms.TextBox txt_ApellidoMaterno;
        public System.Windows.Forms.TextBox txt_Nombre;
        public System.Windows.Forms.TextBox txt_ApellidoPaterno;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDer;
        public System.Windows.Forms.PictureBox pictureBoxDer;
        private System.Windows.Forms.Button btnIzq;
        public System.Windows.Forms.PictureBox pictureBoxIzq;
        private System.Windows.Forms.Panel Status;
        private System.Windows.Forms.Label Lbl_status;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pbLoading;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button buttonAccessUpdate;
        private System.Windows.Forms.GroupBox groupBoxEmployee;
        private System.Windows.Forms.PictureBox pictureBoxEmployeeStatus;
        private System.Windows.Forms.Button buttonUpdateEmployee;
        private System.Windows.Forms.Button buttonDiscardFingerprints;
        private System.Windows.Forms.Button buttonConnectDevice;
        private System.Windows.Forms.Button buttonDisconnectDevice;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label labelFingerRight;
        private System.Windows.Forms.Label labelFingerLeft;
    }
}