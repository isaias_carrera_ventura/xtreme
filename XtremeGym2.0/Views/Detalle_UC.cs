﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;

namespace XtremeGym2._0
{
    public partial class Detalle_UC : UserControl
    {
        private int id_cliente;
        private Modelo mimodelo;

        public Modelo Mimodelo { get => mimodelo; set => mimodelo = value; }
        public int Id_cliente { get => id_cliente; set => id_cliente = value; }

        Panel p;
        Button btn;
        UserControl home;
        String templeteString= null;
        String templeteString2= null;

        int idtemplate1 = 0;
        int idtemplate2 = 0;

        fingerprint fp;
        

        Panel status;
        Label lbl_status;

        public Panel P { get => p; set => p = value; }
        public Button Btn { get => btn; set => btn = value; }
        public UserControl Home { get => home; set => home = value; }
        public Panel Status { get => status; set => status = value; }
        public Label Lbl_status { get => lbl_status; set => lbl_status = value; }
        public PictureBox pb1 { get => pictureBox1; set => pictureBox1 = value; }
        public fingerprint Fp { get => fp; set => fp = value; }
        public string TempleteString { get => templeteString; set => templeteString = value; }
        public string TempleteString2 { get => templeteString2; set => templeteString2 = value; }
        public int Idtemplate1 { get => idtemplate1; set => idtemplate1 = value; }
        public int Idtemplate2 { get => idtemplate2; set => idtemplate2 = value; }

        public Detalle_UC()
        {
            InitializeComponent();
        }
        


        private void Detalle_UC_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }

        private void lbl_registro_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //SCAN IZQUIERDO
            Fp.PictureBox = pictureBox1;
            try
            {
                Status.BackColor = Color.Orange;

                Lbl_status.Text = "Esperando Huella Digital...";
                Fp.Fp_reg = "";
                Fp.Error = 0;
                Fp.Registro();

                while (Fp.Fp_reg == "")
                {
                    
                    if (Fp.Error==-22)
                    {
                        MessageBox.Show("Se Colocaron Huellas Diferentes, Intentelo de Nuevo.");
                        pictureBox1.Image = null;
                        break;
                    }
                }
                

                if (Fp.Error==0 && Fp.Fp_reg!=null) {
                    templeteString = Fp.Fp_reg;
                    Status.BackColor = Color.LightSteelBlue;
                    Lbl_status.Text = "Huella Capturada, sin acciones.";
                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                fingerprint.captureThread.Suspend();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //SCAN DERECHO
            Fp.PictureBox = pictureBox3;
            try
            {
                Status.BackColor = Color.Orange;

                Lbl_status.Text = "Esperando Huella Digital...";
                Fp.Fp_reg = "";
                Fp.Error = 0;
                Fp.Registro();

                while (Fp.Fp_reg == "")
                {

                    if (Fp.Error == -22)
                    {
                        MessageBox.Show("Se Colocaron Huellas Diferentes, Intentelo de Nuevo.");
                        pictureBox3.Image = null;
                        break;
                    }
                }


                if (Fp.Error == 0 && Fp.Fp_reg != null)
                {
                    templeteString2 = Fp.Fp_reg;
                    Status.BackColor = Color.LightSteelBlue;
                    Lbl_status.Text = "Huella Capturada, sin acciones.";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                fingerprint.captureThread.Suspend();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.button2.Enabled = false;

            if (mimodelo.Validacion2(this) != true)
            {
                    if (mimodelo.Correo_query(txt_Correo.Text,"editar"))
                    {
                    string nombre = txt_Nombre.Text;
                    string ap = txt_ApellidoPaterno.Text;
                    string am = txt_ApellidoMaterno.Text;
                    int edad = Convert.ToInt16(txt_edad.Text);
                    string sexo = "";

                    if (radio_sm.Checked)
                    {
                        sexo = "M";
                    }
                    else if (radio_sf.Checked)
                    {
                        sexo = "F";
                    }

                    string correo = txt_Correo.Text;
                    string telefono = txt_Telefono.Text;
                    DateTime año = DateTime.Now;
                    string clave = "Password" + año.Year;
                    clave = mimodelo.Base64Encode(clave);
                    int orden = 1;
                    int status = 1;

                    string foto64 = null;
                    if (pictureBox2.Image != null)
                    {
                        foto64 = ConvertImageToBase64String(pictureBox2.Image);
                    }

                    string calle = txt_Calle.Text;
                    string numero = txt_Numero.Text;
                    string colonia = txt_Colonia.Text;
                    string cp = txt_CP.Text;
                    string ciudad = txt_Ciudad.Text;

                    if ((pictureBox1.Image == null && pictureBox3.Image != null) && (pictureBox1.Image != null && pictureBox3.Image == null))
                    {
                        MessageBox.Show("Se deben agregar ambas huellas, no solo una.");
                        return;
                    }
                    else
                    {
                        if (mimodelo.Actualizar_Usuario_Cliente(id_cliente, idtemplate1, idtemplate2, foto64, nombre, ap, am, edad, sexo, correo, telefono, calle, numero, colonia, cp, ciudad, clave, orden, status, templeteString, templeteString2))
                        {

                            this.button2.Enabled = true;
                            mimodelo.Cancelar(this);
                            pictureBox1.Image = null;
                            pictureBox2.Image = null;
                            pictureBox3.Image = null;
                            templeteString = null;
                            templeteString2 = null;
                            P.Top = Btn.Top;
                            idtemplate1 = 0;
                            idtemplate2 = 0;
                            id_cliente = 0;
                            Home.BringToFront();
                        }
                        else
                        {
                            MessageBox.Show("Fracaso al agregar a la BD");
                            this.button2.Enabled = true;
                        }
                    }
                }
                    else
                    {
                        MessageBox.Show("El correo ya existe, intente de nuevo.");
                        this.button2.Enabled = true;
                    }
                
            }
            else
            {
                this.button2.Enabled = true;
            }
        }

        public string ConvertImageToBase64String(Image image)
        {
            var imageStream = new MemoryStream();
            image.Save(imageStream, ImageFormat.Png);
            imageStream.Position = 0;
            var imageBytes = imageStream.ToArray();
            return Convert.ToBase64String(imageBytes);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Status.BackColor = Color.Orange;
            Lbl_status.Text = "Esperando Captura de Foto";
            //CapturaFoto cf = new CapturaFoto(pictureBox2, Status, Lbl_status);
            CapturaFoto cf = new CapturaFoto(pictureBox2, Status, Lbl_status,0);
            cf.Show();
        }
    }
}
