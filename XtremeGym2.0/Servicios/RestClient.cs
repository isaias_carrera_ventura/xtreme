﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace XtremeGym2._0.Servicios
{
    class RestClient
    {
        const string baseUrl = "https://xtremegym.com.mx/gimnasio/Appws/";

        public static async Task<string> SubirFoto(string foto, string id)
        {
            string res;
            /*var formContent = new FormUrlEncodedContent(new[]
           {
                   new KeyValuePair<string,string>("foto", foto),
                   new KeyValuePair<string,string>("id", id)
           });*/
            
            var formContent = new
            {
                foto = foto,
                id = id
            };
            var stringPayload = JsonConvert.SerializeObject(formContent);
            var content = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "ActualizarFoto", content);

            if (response != null)
            {
                res = response.Content.ReadAsStringAsync().Result;
            }
            else {
                res = "No se obtuvo una respuesta del servidor";
            }

            return res;
        }

        public static async Task<string> SubirIdentificacion(string foto, string id)
        {
            string res;
            /*var formContent = new FormUrlEncodedContent(new[]
           {
                   new KeyValuePair<string,string>("foto", foto),
                   new KeyValuePair<string,string>("id", id)
           });*/

            var formContent = new
            {
                foto = foto,
                id = id
            };
            var stringPayload = JsonConvert.SerializeObject(formContent);
            var content = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "ActualizarIdentificacion", content);

            if (response != null)
            {
                res = response.Content.ReadAsStringAsync().Result;
            }
            else
            {
                res = "No se obtuvo una respuesta del servidor";
            }

            return res;
        }

    }
}
